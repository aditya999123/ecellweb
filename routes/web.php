<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('welcome');
Route::get('/vision', 'HomeController@vision');
Route::get('/team','HomeController@team');
Route::get('/blogs','HomeController@blogs');
Route::get('/bmodel','HomeController@bmodel');
Route::get('/verify','HomeController@verify');
Route::get('/resend_otp','HomeController@resendOtp')->name('resend_otp_web');
Route::post('/verify_otp_web','HomeController@verifyOtp')->name('verify_otp_web');
Route::post('/send_otp_web','HomeController@sendOtp')->name('send_otp_web');
Route::get('/logout', 'HomeController@logout')->name('logout');

// Route::resource('login', 'LoginController');

// Route::resource('signup', 'SignupController');
Route::get('/profile','HomeController@my_profile')->name('my_pofile');

Route::get('/events/ES2k1700{id}','EventsController@show');
Route::get('/events/get_event_detail/{id}','EventsController@getEventDetail');
Route::post('/events/update_event', 'EventsController@update')->name('update_event');
Route::get('/events/approve_event/{id}','EventsController@approveEvent');
Route::get('/events/unapprove_event/{id}','EventsController@unapproveEvent');
Route::get('/get_events_list','EventsController@getEventsList')->name('get_events_list');

Route::get('/sponsors/SPONS00{id}','SponsorsController@show');
Route::get('/sponsors/get_sponsor_detail/{id}','SponsorsController@getSponsorDetail');
Route::post('/sponsors/update_sponsor', 'SponsorsController@update')->name('update_sponsor');
Route::get('/sponsors/approve_sponsor/{id}','SponsorsController@approveSponsor');
Route::get('/sponsors/unapprove_sponsor/{id}','SponsorsController@unapproveSponsor');
Route::get('/get_sponsors_list','SponsorsController@getSponsorsList')->name('get_sponsors_list');

Route::get('/startups/STRTUP00{id}','StartupsController@show');
Route::get('/startups/get_startup_detail/{id}','StartupsController@getStartupDetail');
Route::post('/startups/update_startup', 'StartupsController@update')->name('updateStartup');
Route::get('/startups/approve_startup/{id}','StartupsController@approveStartup');
Route::get('/startups/unapprove_startup/{id}','StartupsController@unapproveStartup');
Route::get('/get_startups_list','StartupsController@getStartupsList');

Route::get('/speakers/SPKR00{id}','SpeakersController@show');
Route::get('/speakers/get_speaker_detail/{id}','SpeakersController@getSpeakerDetail');
Route::post('/speakers/update_speaker', 'SpeakersController@update')->name('updateSpeaker');
Route::get('/speakers/approve_speaker/{id}','SpeakersController@approveSpeaker');
Route::get('/speakers/unapprove_speaker/{id}','SpeakersController@unapproveSpeaker');
Route::get('/get_speakers_list','SpeakersController@getSpeakersList')->name('get_speakers_list');

Route::post('/team_members/update_team_member', 'TeamMembersController@update')->name('updateTeamMember');
Route::get('/team_members/get_team_member_detail/{id}','TeamMembersController@getTeamMemberDetail');
Route::get('/team_members/approve_team_member/{id}','TeamMembersController@approveTeamMember');
Route::get('/team_members/unapprove_team_member/{id}','TeamMembersController@unapproveTeamMember');
Route::get('/get_team_members_list','TeamMembersController@getTeamMembersList')->name('get_team_members_list');

Route::get('/questionSets/approve_question_set/{id}','QuestionSetsController@approveQuestionSet');
Route::get('/questionSets/unapprove_question_set/{id}','QuestionSetsController@unapproveQuestionSet');
Route::get('/questionSets/get_question_set_detail/{id}','QuestionSetsController@getQuestionSetDetail');
Route::get('/get_question_sets_list','QuestionSetsController@getQuestionSetsList');

Route::get('/questions/get_question_detail/{id}','QuestionsController@getQuestionDetail');
Route::get('/questions/get_questions_list','QuestionsController@getQuestionsList');
Route::get('/questions/unapprove_question/{id}','QuestionsController@unapproveQuestion');
Route::get('/questions/approve_question/{id}','QuestionsController@approveQuestion');

Route::get('/app_versions/unapprove_version/{id}','AppVersionController@unapproveVersion');
Route::get('/app_versions/approve_version/{id}','AppVersionController@approveVersion');

Route::get('/mentors/SPKR00{id}','MentorsController@show');
Route::get('/mentors/get_mentor_detail/{id}','MentorsController@getMentorDetail');
Route::post('/mentors/update_mentor', 'MentorsController@update')->name('updateMentor');
Route::get('/mentors/approve_mentor/{id}','MentorsController@approveMentor');
Route::get('/mentors/unapprove_mentor/{id}','MentorsController@unapproveMentor');
Route::get('/get_mentors_list','MentorsController@getMentorsList')->name('get_mentors_list');

Route::get('/get_team_members_list','TeamMembersController@getTeamMembersList')->name('get_team_members');

//Route::middleware('auth_panel')->group(function () {

Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/admin_panel', 'HomeController@admin_panel')->name('admin_panel');

// Route::get('/coordinator_panel', 'HomeController@coordinator_panel')->name('coordinator_panel');

// Route::get('/manager_panel', 'HomeController@manager_panel')->name('manager_panel');

// Route::get('/executive_panel', 'HomeController@executive_panel')->name('executive_panel');

// Route::get('/ambassador_panel', 'HomeController@ambassador_panel')->name('ambassador_panel');

//});
// Route::get('/admin','AdminController@index')->name('admin');

Route::get('/admin/events','AdminController@events')->name('admin_events');
Route::get('/admin/events/add','AdminController@addEvent')->name('add_events_admin');
Route::get('/admin/events/edit/{id}','AdminController@editEvent')->name('edit_events_admin');
Route::get('/admin/sponsors','AdminController@sponsors')->name('admin_sponsors');
Route::get('/admin/startups','AdminController@startups')->name('admin_startups');
Route::get('/admin/users','AdminController@users')->name('admin_users');
Route::get('/admin/show_speakers','AdminController@speakers')->name('admin_speakers');
Route::get('/admin/approve_question_sets','AdminController@questionSets')->name('admin_question_sets');
Route::get('/admin/approve_questions','AdminController@questions')->name('admin_questions');
Route::post('/admin/send_bulk_message','AdminController@sendBulkMessage')->name('send_bulk_messages');
Route::get('/admin/mentors','AdminController@mentors')->name('admin_mentors');
Route::get('/admin/show_app_versions','AdminController@appVersions')->name('admin_app_version');
Route::post('/admin/add_versions','AdminController@addVersion')->name('admin_add_app_version');
Route::get('/admin/show_team_members','AdminController@teamMembers')->name('admin_team_members');
Route::get('/admin/contact_us','AdminController@contactUs')->name('admin_contact_us');
Route::get('/admin/get_question_sets','QuestionSetsController@getAdminList')->name('admin_get_question_sets_api');
Route::get('/admin/get_questions','QuestionsController@getAdminList')->name('admin_get_questions_api');
Route::get('/admin/get_question_sets_for_question','QuestionSetsController@getAdminListForQuestion')->name('admin_questions_list_for_question');
Route::get('/admin/export_to_excel','AdminController@exportToExcel')->name('export_to_excel');
Route::get('/admin/approve_answers','AdminController@showAnswers')->name('show_admin_answers');
Route::get('/admin/leaderboard','AdminController@showLeaderboard')->name('show_admin_leaderboard');

Route::get('/admin/bquiz/approve_answer/{id}','AdminController@approveAnswer')->name('approve_answer');
Route::get('/admin/bquiz/unapprove_answer/{id}','AdminController@unapproveAnswer')->name('unapprove_answer');

Route::get('/get_blogs_list','BlogsController@getBlogsList')->name('blogs_list');
Route::get('/blogs/{slug}','BlogsController@showBlog')->name('show_blog');
Route::get('/pages/get_pages_list','PagesController@getPagesList')->name('pages_list');

Route::post('/submit_contact_us','ContactUsController@contactUs')->name('contact_us_api');
// Route::post('/submit_blog','BlogsController@submitBlog')->name('submit_blog');
Route::post('/create_user','AdminController@createUser')->name('create_user');
Route::post('/verify_otp','AdminController@verify_otp')->name('verify_otp');
Route::post('/submit_answer_app','BQuizController@submitAnswerApp')->name('submit_answer_bquiz_app');
Route::post('/submit_answer_web','BQuizController@submitAnswerWeb')->name('submit_answer_bquiz_web');
Route::get('/get_live_question','BQuizController@getQuestion')->name('get_live_question');
Route::get('/get_live_question_web','BQuizController@getQuestionWeb')->name('get_live_question');
Route::post('/is_update_available','HomeController@submitFcm')->name('submit_fcm');

Route::get('/is_bquiz_active','BQuizController@activeOrNot')->name('bquiz_status');

// Route::post('/is_update_available','AppVersionController@isUpdateAvailable')->name('is_update_available');

Route::get('/bquiz','BQuizController@index');

Route::get('/profile','HomeController@profile');

Route::resource('startups', 'StartupsController');

Route::resource('sponsors', 'SponsorsController');

Route::resource('events', 'EventsController');

Route::resource('speakers','SpeakersController');

Route::resource('questionSets','QuestionSetsController');

Route::resource('questions','QuestionsController');

Route::resource('mentors','MentorsController');

Route::resource('team_members','TeamMembersController');

Auth::routes();

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
