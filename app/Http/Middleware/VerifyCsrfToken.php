<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/submit_contact_us',
        // '/submit_blog',
        '/create_user',
        '/verify_otp',
        '/submit_answer_app',
        '/submit_fcm',
        '/is_update_available'
    ];
}
