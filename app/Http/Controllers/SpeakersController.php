<?php 

namespace App\Http\Controllers;
use Auth;
use App\Speaker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SpeakersController extends Controller{
    
    public function __construct(){
        $this->middleware('auth', ['except' => ['index', 'show','getSpeakerDetail','getSpeakersList']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('speakers');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $speaker = new Speaker;
        $speaker->name = $request->name;
        $speaker->description = $request->description;
        $speaker->contact_no = $request->contact_no;
        $speaker->contact_email = $request->contact_email;
        $speaker->year = $request->year;
        $speaker->user_id = Auth::id();
        $metaName = time().'.'.$request->meta->getClientOriginalExtension();
        $request->meta->move(public_path('uploads/speakers'), $metaName);
        $speaker->meta = $metaName;

        if($speaker->save()) {
            return redirect()->route('admin_speakers');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Speakers  $speakers
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $len = strlen($id);
        $speaker_id = intval(substr($id, 7));        
        $speaker = DB::table('speakers')->where([
                ['speaker_id', '=', $speaker_id ],
                ['status','=','approved']
            ])->get();
        return view('speakers.show', ['speaker' => $speaker]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Speakers  $speakers
     * @return \Illuminate\Http\Response
     */
    public function edit(Speaker $speakers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Speakers  $speakers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->meta == null){
            Speaker::where('speaker_id', '=' ,$id)
                ->update([
                    'name' => $request->name,
                    'description' => $request->description,
                    'contact_no' => $request->contact_no,
                    'contact_email' => $request->contact_email,
                    'year' => $request->year,
                    'user_id' => Auth::id(),
                ]);
        }else{
            $metaName = time().'.'.$request->meta->getClientOriginalExtension();
            Speaker::where('speaker_id', '=' ,$id)
                ->update([
                    'name' => $request->name,
                    'description' => $request->description,
                    'contact_no' => $request->contact_no,
                    'contact_email' => $request->contact_email,
                    'year' => $request->year,
                    'user_id' => Auth::id(),
                    'meta' => $metaName,
                ]);
            $metaName = time().'.'.$request->meta->getClientOriginalExtension();
            $request->meta->move(public_path('uploads/speakers'), $metaName);
        }
        return redirect()->route('admin_speakers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Speakers  $speakers
     * @return \Illuminate\Http\Response
     */
    public function destroy(Speaker $speaker)
    {
        //
    }

    public function getSpeakerDetail($id){
        $speaker = DB::table('speakers')->where('speaker_id', '=', $id)->get();
        return response()->json([
            'speaker_id' => $speaker[0]->speaker_id,
            'speaker_name' => $speaker[0]->name,
            'speaker_description'=> $speaker[0]->description,
            'speaker_pic' => $speaker[0]->meta,
            'speaker_contact_no' => $speaker[0]->contact_no,
            'speaker_contact_email' => $speaker[0]->contact_email,
            'speaker_year' => $speaker[0]->year
        ]);
    }
    
    public function approveSpeaker($id){
        $user = Auth::user();
        if($user->user_type == "ADMIN"){
            try{
                $temp_event = Speaker::where('speaker_id',$id)
                            ->update(['status' => 'approved']);
                return response()->json([
                    'flag' => true,
                    'message' => 'Speaker has been approved successfully'
                ]);
            }
            catch (Exception $e) {
                return response()->json([
                    'flag' => false,
                    'message' => 'Sorry an error occured'
                ]);   
            }
        }
        else {
            return response()->json([
                    'flag' => false,
                    'message' => 'You do not have sufficient privilage'
                ]);   
        }
    }

    public function unapproveSpeaker($id){
        $user = Auth::user();
        if($user->user_type == "ADMIN"){
            try{
                $temp_event = Speaker::where('speaker_id',$id)
                            ->update(['status' => 'unapproved']);
                return response()->json([
                    'flag' => true,
                    'message' => 'Event has been unapproved successfully you can approve it again whenever you want'
                ]);
            }
            catch (Exception $e) {
                return response()->json([
                    'flag' => false,
                    'message' => 'Sorry an error occured'
                ]);   
            }
        }
        else {
            return response()->json([
                    'flag' => false,
                    'message' => 'You do not have sufficient privilage'
                ]);   
        }
    }

    public function getSpeakersList(Request $request){
        $speakers = DB::table('speakers')
                    ->where('status','=','approved')
                    ->orderBy('year', 'desc')
                    ->get();
        if($speakers->count() != 0){
            $temp_speakers = $speakers->all();
            $speakers = array();
            foreach($temp_speakers as $speaker){
                $temp = array(
                    'name' => $speaker->name,
                    'description' => $speaker->description,
                    'image' => $request->getSchemeAndHttpHost().'/uploads/speakers/'.$speaker->meta,
                    'year' => $speaker->year,
                );
                array_push($speakers,$temp);
            }
            $data = array(
                'success' => True,
                'message' => 'Hiya I was called',
                'speakers' => $speakers,
            );
        }else{
            $data = array(
                'success' => False,
                'message' => 'Coming Soon'
            );
        }
        return $data;
    }
}
