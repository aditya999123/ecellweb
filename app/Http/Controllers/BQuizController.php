<?php

namespace App\Http\Controllers;

use App\Event;
use App\User;
use App\Speaker;
use App\Sponsor;
use App\Answer;
use App\Question;
use App\QuestionSet;
use Lcobucci\JWT\Parser;
// use App\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class BQuizController extends Controller
{

    public function __construct(){
        $this->middleware('auth',['except' => ['activeOrNot','getQuestion','submitAnswerApp']]);
    }

    public function index(){
        $data = array(
            'events' => Event::where('status','approved')->count(),
            'users' => User::where('status','approved')->count(),
            'speakers' => Speaker::where('status','approved')->count(),
            'sponsors' => Sponsor::where('status','approved')->count()
        );
        return view('bquiz.index',$data);
    }

    public function activeOrNot(){
        $count = QuestionSet::where('status','approved')->count();
        if($count == 0){
            $data = array(
                'success' => false,
                'message' => setting('bquiz_not_active')
            );
        }else{
            $data = array(
                'success' => true,
                'message' => setting('bquiz_active')
            );
        }
        return $data;
    }

    public function getQuestion(Request $request){        
        $count = QuestionSet::where('status','approved')->count();
        if($count > 0){
            $count = Question::where('status','approved')->count();
            if($count > 0){
                $question = Question::where('status','approved')->get();
                $time = $question[0]->timelimit;
                $time = explode(':',$time);
                $seconds = ((int)$time[0]*60)+(int)$time[1];
                $rules = "Special round of B-Quiz powered by Chhattisgarh Tourism consists of 10 questions.\n";
                $rules .= "Every Question will be live for 3 minutes.\n";
                $rules .= "B-Quiz will start at 9:15 PM Today.\n";
                $rules .= "The decision of the E-Cell NIT Raipur will be final and will not be subjected to any change.\n";
                $rules .= "Audience  shall  not give any hints or clues to the competitors.";
                if($question[0]->question_type == 'text'){
                    $data_type = 1;
                }else{
                    $data_type = 2;
                }
                $question = array(
                    'question_id' => $question[0]->question_id,
                    'question' => $question[0]->description,
                    'image_url' => $request->getSchemeAndHttpHost().'/uploads/questions/'.$question[0]->meta,
                    'points' => $question[0]->score,
                    'question_duration' => $seconds,
                );
                $data = array(
                    'success' => true,
                    'message' => "Questions are available",
                    'message_image_url' => "Questions are available",
                    'data_type' => $data_type,
                    'rules' => $rules,
                    'question_data' => $question
                );
                
                // This was in bottom meghal added it here
                if($request->access_token != null){
                    Log::info('Got access token '.$request->access_token);
                    $token = (new Parser())->parse((string) $request->access_token);
                    $data_in_token = $token->getClaims();
                    $user_id = DB::table('users')->where('contact_no', $data_in_token['contact_no'])->value('id');
                    // var_dump($user);
                    // $user_id = $user->id;
                    $question = Question::where('status','approved')->get();
                    $count = Answer::where('question_id',$question[0]->question_id)
                                ->where('user_id',$user_id)
                                ->count();
                    if($count > 0){
                        $data = array(
                            'success' => false,
                            'message' => 'Hey! You have already answered! Please wait for some time or tell your friends to Download ECell app and compete with you! :P',
                            'message_image_url' => 'http://www.pfpenergy.co.uk/media/1184/help-and-support.png',
                            'message_display' => 'Sorry you have already answered please wait for some time',
                        );
                    }
                }

            }else{
                $data = array(
                    'success' => false,
                    'message' => "We have received a tremendous response for the last Quiz! Thanks for your participation! Next Quiz on Wednesday - 30 August 2017 !! Be Ready, Tell your friends to Download E Cell App and Login to win exciting prize!",
                    'message_image_url' => 'http://www.pfpenergy.co.uk/media/1184/help-and-support.png',

                );
            }


        }else{
            $data = array(
                'success' => false,
                'message' => "Coming Soon",
                'message_image_url' => 'http://www.pfpenergy.co.uk/media/1184/help-and-support.png',

            );
        }

        // Checking if we are getting a token or not - The above code was actually here
        
        return $data;
    }

    public function getQuestionWeb(Request $request){
        try{
            $count = QuestionSet::where('status','approved')->count();
            if($count > 0){
                $count = Question::where('status','approved')->count();
                if($count > 0){
                    $question = Question::where('status','approved')->get();
                    $user = Auth::user();
                    $user_id = $user->id;
                    $answer_count = Answer::where('user_id',$user_id)
                                            ->where('question_id',$question[0]->question_id)
                                            ->count();
                    if($answer_count == 0){
                        $time = $question[0]->timelimit;
                        $time = explode(':',$time);
                        $seconds = ((int)$time[0]*60)+(int)$time[1];
                        $rules = "The decision of the E-Cell NIT Raipur will be final and will not be subjected to any change.\n";
                        $rules .= "Audience  shall  not give any hints or clues to the competitors.";
                        if($question[0]->question_type == 'text'){
                            $data_type = 1;
                        }else{
                            $data_type = 2;
                        }
                        $question = array(
                            'question_id' => $question[0]->question_id,
                            'question' => $question[0]->description,
                            'image_url' => $request->getSchemeAndHttpHost().'/uploads/questions/'.$question[0]->meta,
                            'points' => $question[0]->score,
                            'question_duration' => $seconds,
                        );
                        $data = array(
                            'success' => true,
                            'message' => "Questions are available",
                            'message_image_url' => "Questions are available",
                            'data_type' => $data_type,
                            'rules' => $rules,
                            'question_data' => $question
                        ); 
                    }else{
                        $data = array(
                            'success' => false,
                            'message' => 'Please wait for the next Question'
                        );
                    }
                }else{
                    $data = array(
                        'success' => false,
                        'message' => "Please wait for the next question",
                        'message_image_url' => 'http://www.pfpenergy.co.uk/media/1184/help-and-support.png',
    
                    );
                }
            }else{
                $data = array(
                    'success' => false,
                    'message' => "Coming Soon",
                    'message_image_url' => 'http://www.pfpenergy.co.uk/media/1184/help-and-support.png',
    
                );
            }
        }
        catch(\Exception $e){
            Log::error('Error '.$e->getMessage());
            $data = array(
                'success' => false,
                'message' => 'Sorry an error occured'
            );
        }
        // Checking if we are getting a token or not - The above code was actually here
        
        return $data;
    }

    public function submitAnswerApp(Request $request){
        try{
            $token = (new Parser())->parse((string) $request->access_token);
            $data = $token->getClaims();
            $user = User::where('contact_no',$data['contact_no'])->get();
            $user_id = $user[0]->id;
            $count = Answer::where('question_id',$request->question_id)
                    ->where('user_id',$user_id)
                    ->count();
            if($count == 0){
                $answer = new Answer;
                $answer->answer = $request->answer;
                $answer->question_id = $request->question_id;
                $answer->user_id = $user_id;
                if($answer->save()){
                    $data = array(
                        'success' => true,
                        'message' => "Answer submited successfully",
                        'message_image_url' => "Questions are available",
                        'messageg_display' => "Answer submited successfully",
                    );
                }else{
                    $data = array(
                        'success' => false,
                        'message' => "An error occured while storing your response",
                        'message_image' => "Questions are available",
                        'message_display' => "An error occured while storing your response",
                    );
                }
            }else{
                $data = array(
                    'success' => false,
                    'message' => "You cannot submit answer again",
                    'message_image' => "Questions are available",
                    'message_display' => "You cannot submit answer again",
                );
            }
        }catch(\Exception $e){
            Log::error('Error occured '.$e->getMessage());
            $data = array(
                'success' => false,
                'message' => "Sorry an error occured",
                'message_image' => "Sorry an error occured",
                'message_display' => "Sorry an error occured",
            );
        }
        return $data;
    }

    public function submitAnswerWeb(Request $request){
        try{
            $user = Auth::user();
            $user_id = $user->id;
            $count = Answer::where('question_id',$request->question_id)
                    ->where('user_id',$user_id)
                    ->count();
            Log::info('User answering question '.$user);
            if($count == 0){
                Log::info('User has not answered the question '.$user);
                $answer = new Answer;
                $answer->answer = $request->answer;
                $answer->question_id = $request->question_id;
                $answer->user_id = $user_id;
                if($answer->save()){
                    Log::info('Answer Saved '.$answer);
                    $data = array(
                        'success' => true,
                        'message' => "Your response has been recorded successfully",
                        'message_image_url' => "Questions are available",
                        'messageg_display' => "Answer subumited successfully",
                    );
                }else{
                    Log::info('Cannot save answer '.$answer);
                    $data = array(
                        'success' => false,
                        'message' => "An error occured while storing your response",
                        'message_image' => "Questions are available",
                        'message_display' => "An error occured while storing your response",
                    );
                }
            }else{
                Log::info('User has already answered the question '.$user);
                $data = array(
                    'success' => false,
                    'message' => "You cannot submit answer again",
                    'message_image' => "Questions are available",
                    'message_display' => "You cannot submit answer again",
                );
            }
        }catch(\Exception $e){
            Log::error('Exception '.$e->getMessage());
            $data = array(
                'success' => false,
                'message' => 'Sorry an error occured please try again later'
            );
        }
        return $data;
    }
}
