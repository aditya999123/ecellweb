<?php 

namespace App\Http\Controllers;
use Auth;
use App\Mentor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MentorsController extends Controller{
    
    public function __construct(){
        $this->middleware('auth', ['except' => ['index', 'show','getMentorDetail','getMentorsList']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('mentors.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $mentor = new Mentor;
        $mentor->name = $request->name;
        $mentor->description = $request->description;
        $mentor->details = $request->details;
        $mentor->contact_no = $request->contact_no;
        $mentor->contact_email = $request->contact_email;
        $mentor->user_id = Auth::id();
        $metaName = time().'.'.$request->meta->getClientOriginalExtension();
        $request->meta->move(public_path('uploads/mentors'), $metaName);
        $mentor->meta = $metaName;

        if($mentor->save()) {
            return redirect('/admin/mentors');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mentors  $mentors
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $len = strlen($id);
        $mentor_id = intval(substr($id, 7));        
        $mentor = DB::table('mentors')->where([
                ['mentor_id', '=', $mentor_id ],
                ['status','=','approved']
            ])->get();
        return view('mentors.show', ['mentor' => $mentor]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mentors  $mentors
     * @return \Illuminate\Http\Response
     */
    public function edit(Mentor $mentors)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mentors  $mentors
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->meta == null){
            Mentor::where('mentor_id', '=' ,$id)
                ->update([
                    'name' => $request->name,
                    'description' => $request->description,
                    'details' => $request->details,
                    'contact_no' => $request->contact_no,
                    'contact_email' => $request->contact_email,                    
                    'user_id' => Auth::id(),
                ]);
        }else{
            $metaName = time().'.'.$request->meta->getClientOriginalExtension();
            Mentor::where('mentor_id', '=' ,$id)
                ->update([
                    'name' => $request->name,
                    'description' => $request->description,
                    'details' => $request->details,
                    'contact_no' => $request->contact_no,
                    'contact_email' => $request->contact_email,
                    'user_id' => Auth::id(),
                    'meta' => $metaName,
                ]);
            $metaName = time().'.'.$request->meta->getClientOriginalExtension();
            $request->meta->move(public_path('uploads/mentors'), $metaName);
        }
        return redirect('/admin/mentors');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mentors  $mentors
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mentor $mentor)
    {
        //
    }

    public function getMentorDetail($id){
        $mentor = DB::table('mentors')->where('mentor_id', '=', $id)->get();
        return response()->json([
            'mentor_id' => $mentor[0]->mentor_id,
            'mentor_name' => $mentor[0]->name,
            'mentor_description'=> $mentor[0]->description,
            'mentor_pic' => $mentor[0]->meta,
            'mentor_contact_no' => $mentor[0]->contact_no,
            'mentor_contact_email' => $mentor[0]->contact_email,
        ]);
    }
    
    public function approveMentor($id){
        $user = Auth::user();
        if($user->user_type == "ADMIN"){
            try{
                $temp_event = Mentor::where('mentor_id',$id)
                            ->update(['status' => 'approved']);
                return response()->json([
                    'flag' => true,
                    'message' => 'Mentor has been approved successfully'
                ]);
            }
            catch (Exception $e) {
                return response()->json([
                    'flag' => false,
                    'message' => 'Sorry an error occured'
                ]);   
            }
        }
        else {
            return response()->json([
                    'flag' => false,
                    'message' => 'You do not have sufficient privilage'
                ]);   
        }
    }

    public function unapproveMentor($id){
        $user = Auth::user();
        if($user->user_type == "ADMIN"){
            try{
                $temp_event = Mentor::where('mentor_id',$id)
                            ->update(['status' => 'unapproved']);
                return response()->json([
                    'flag' => true,
                    'message' => 'Event has been unapproved successfully you can approve it again whenever you want'
                ]);
            }
            catch (Exception $e) {
                return response()->json([
                    'flag' => false,
                    'message' => 'Sorry an error occured'
                ]);   
            }
        }
        else {
            return response()->json([
                    'flag' => false,
                    'message' => 'You do not have sufficient privilage'
                ]);   
        }
    }

    public function getMentorsList(Request $request){
        $mentors = DB::table('mentors')->where(
                'status','=','approved'
            )->get();
        if($mentors->count() != 0){
            $temp_mentors = $mentors->all();
            $mentors = array();
            foreach($temp_mentors as $mentor){
                $temp = array(
                    'name' => $mentor->name,
                    'description' => $mentor->description,
                    'details' => $mentor->details,
                    'image' => $request->getSchemeAndHttpHost().'/uploads/mentors/'.$mentor->meta,
                );
                array_push($mentors,$temp);
            }
            $data = array(
                'success' => True,
                'message' => 'Hiya I was called',
                'mentors' => $mentors,
            );
        }else{
            $data = array(
                'success' => False,
                'message' => 'Coming Soon'
            );
        }
        return $data;
    }
}
