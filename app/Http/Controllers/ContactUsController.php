<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactUs;

class ContactUsController extends Controller
{
    public function contactUs(Request $request){
        $message = new ContactUs;
        if($request->subject == null){
            $temp_subject = 'Default';
        }else{
            $temp_subject = $request->subject;
        }
        $message->name = $request->name;
        $message->email = $request->email;
        $message->subject = $temp_subject;
        $message->message = $request->message;
        if($message->save()) {
            $data = array(
                'success' => true,
                'message' => "Your response has successfully been recorded."
            );
        }else{
            $data = array(
                'success' => false,
                'message' => "Sorry an error occured we can not process your request"
            );
        } 
        return $data;
    }
}
