<?php 

namespace App\Http\Controllers;
use Auth;
use App\TeamMember;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TeamMembersController extends Controller{
    
    public function __construct(){
        $this->middleware('auth', ['except' => ['index', 'show','getSpeakerDetail','getTeamMembersList']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('team');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $team_member = new TeamMember;
        $team_member->name = $request->name;
        $team_member->position = $request->member_position;
        $team_member->linkedIn = $request->linkedIn;
        $metaName = time().'.'.$request->meta->getClientOriginalExtension();
        $request->meta->move(public_path('uploads/team_members'), $metaName);
        $team_member->meta = $metaName;

        if($team_member->save()) {
            return redirect('/admin/team_members');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Speakers  $speakers
     * @return \Illuminate\Http\Response
     */
    // public function show($id){
    //     $speaker = DB::table('team_members')->where([
    //             ['id', '=', $id ],
    //             ['status','=','approved']
    //         ])->get();
    //     return view('speakers.show', ['speaker' => $speaker]);
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Speakers  $speakers
     * @return \Illuminate\Http\Response
     */
    public function edit(Speaker $speakers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Speakers  $speakers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->meta == null){
            TeamMember::where('id', '=' ,$id)
                ->update([
                    'name' => $request->name,
                    'linkedin' => $request->linkedIn,
                    'position' => $request->member_position,
                ]);
            $data = array(
                'success' => true,
                'name' => $request->name,
            );
        }else{
            $metaName = time().'.'.$request->meta->getClientOriginalExtension();
            TeamMember::where('id', '=' ,$id)
                ->update([
                    'name' => $request->name,
                    'linkedin' => $request->linkedIn,
                    'position' => $request->member_position,
                    'meta' => $metaName,
                ]);
            $metaName = time().'.'.$request->meta->getClientOriginalExtension();
            $request->meta->move(public_path('uploads/team_members'), $metaName);
            $data = array(
                'success' => false
            );
        }
        return $data;
        // return redirect('/admin/team_members');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Speakers  $speakers
     * @return \Illuminate\Http\Response
     */
    public function destroy(TeamMember $teamMember)
    {
        //
    }

    public function getTeamMemberDetail($id){
        $member = DB::table('team_members')->where('id', '=', $id)->get();
        return response()->json([
            'team_member_id' => $member[0]->id,
            'team_member_name' => $member[0]->name,            
            'team_member_pic' => $member[0]->meta,
            'team_member_linkedIn' => $member[0]->linkedin,
            'team_member_position' => $member[0]->position,
        ]);
    }
    
    public function approveTeamMember($id){
        $user = Auth::user();
        if($user->user_type == "ADMIN"){
            try{
                $temp_event = TeamMember::where('id',$id)
                            ->update(['status' => 'approved']);
                return response()->json([
                    'flag' => true,
                    'message' => 'Member has been approved successfully'
                ]);
            }
            catch (Exception $e) {
                return response()->json([
                    'flag' => false,
                    'message' => 'Sorry an error occured'
                ]);   
            }
        }
        else {
            return response()->json([
                    'flag' => false,
                    'message' => 'You do not have sufficient privilage'
                ]);   
        }
    }

    public function unapproveTeamMember($id){
        $user = Auth::user();
        if($user->user_type == "ADMIN"){
            try{
                $temp_event = TeamMember::where('id',$id)
                            ->update(['status' => 'unapproved']);
                return response()->json([
                    'flag' => true,
                    'message' => 'Member has been unapproved successfully you can approve it again whenever you want'
                ]);
            }
            catch (Exception $e) {
                return response()->json([
                    'flag' => false,
                    'message' => 'Sorry an error occured'
                ]);   
            }
        }
        else {
            return response()->json([
                    'flag' => false,
                    'message' => 'You do not have sufficient privilage'
                ]);   
        }
    }

    public function getTeamMembersList(Request $request){
        $team_members = DB::table('team_members')->where(
                'status','=','approved'
            )->get();
        if($team_members->count() != 0){
            $temp_members = $team_members;
            $final_team_members = array();
            $temp_members = $temp_members->where('position','=','overall');                
            foreach($temp_members as $member){
                $temp = array(
                    'name' => $member->name,
                    'position' => $member->position,
                    'meta' => $request->getSchemeAndHttpHost().'/uploads/team_members/'.$member->meta,
                    'linkedIn' => $member->linkedin
                );
                array_push($final_team_members,$temp);
            }
            $temp_members = $team_members->where('position','=','head');                
            foreach($temp_members as $member){
                $temp = array(
                    'name' => $member->name,
                    'position' => $member->position,
                    'meta' => $request->getSchemeAndHttpHost().'/uploads/team_members/'.$member->meta,
                    'linkedIn' => $member->linkedin
                );
                array_push($final_team_members,$temp);
            }
            $temp_members = $team_members->where('position','=','managers');                
            foreach($temp_members as $member){
                $temp = array(
                    'name' => $member->name,
                    'position' => $member->position,
                    'meta' => $request->getSchemeAndHttpHost().'/uploads/team_members/'.$member->meta,
                    'linkedIn' => $member->linkedin
                );
                array_push($final_team_members,$temp);
            }
            $temp_members = $team_members->where('position','=','executive');                
            foreach($temp_members as $member){
                $temp = array(
                    'name' => $member->name,
                    'position' => $member->position,
                    'meta' => $request->getSchemeAndHttpHost().'/uploads/team_members/'.$member->meta,
                    'linkedIn' => $member->linkedin
                );
                array_push($final_team_members,$temp);
            }
            $data = array(
                'success' => true,
                'message' => 'Team Members are available',
                'team_members' => $final_team_members
            );
        }else{
            $data = array(
                'success' => false,
                'message' => 'Coming Soon'
            );
        }
        return $data;
    }
}
