<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Post;

class BlogsController extends Controller
{
    public function getBlogsList(Request $request){
        $posts_list = DB::table('posts')->where(
                'status','=','PUBLISHED'
            )->get();
        if($posts_list->count() != 0){
            $temp_blogs = $posts_list->all();
            $blogs = array();
            foreach($temp_blogs as $blog){
                $temp = array(
                    'id' => $blog->id,
                    'title' => $blog->title,
                    'body' => $blog->body,
                    'image' => $request->getSchemeAndHttpHost().'/storage/'.$blog->image,
                    'date' => $blog->updated_at,
                    'url' => $request->getSchemeAndHttpHost().'/blogs/'.$blog->slug
                );
                array_push($blogs,$temp);
            }
            shuffle($blogs);
            $data = array(
                'success' => true,
                'message' => "Blogs are available",
                'blogs' => $blogs
            );
        }else{
            $data = array(
                'success' => false,
                'message' => "Coming Soon"
            );
        }
        return $data;
    }

    public function showBlog($slug){
        $blog = Post::where('slug',$slug);
        return view('blogs.show',['blog' => $blog->first()]);        
    }

    // public function submitBlog(Request $request){
    //     $delimiter = '-';
    //     $token = $request->token;
    //     $blog = new Post;
    //     $title = $request->title;
    //     $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $title))))), $delimiter));
    //     $author_id = 
    // }
}
