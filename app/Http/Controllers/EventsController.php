<?php

namespace App\Http\Controllers;
use App\Event;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EventsController extends Controller
{
    
    public function __construct(){
        $this->middleware('auth', ['except' => ['index', 'show','getEventDetail','getEventsList']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $events = DB::table('events')->where(
                'status','=','approved'
            )->get();
        return view('events.index',['events' => $events]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $event = new Event;
        $event->title = $request->title;
        $event->description = $request->description;
        $event->details = $request->details;
        $event->venue = $request->venue;
        $event->date = $request->date;
        $event->time = $request->time;
        $event->user_id = Auth::id();
        $metaName = time().'.'.$request->meta->getClientOriginalExtension();
        $background = time().'bg.'.$request->image_bg->getClientOriginalExtension();
        $request->meta->move(public_path('uploads/events'), $metaName);
        $request->image_bg->move(public_path('uploads/events'), $background);
        $event->meta = $metaName;
        $event->image_bg = $background;

        if($event->save()) {
            return redirect('/admin/events');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){   
        $len = strlen($id);
        $event_id = intval(substr($id, 8));        
        $event = DB::table('events')->where([
                ['event_id', '=', $event_id ],
                ['status','=','approved']
            ])->get();
        return view('events.show', ['event' => $event]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        if($request->meta == null && $request->image_bg == null){
            Event::where('event_id', $id)
                ->update([
                    'title' => $request->title,
                    'description' => $request->description,
                    'details' => $request->details,
                    'venue' => $request->venue,
                    'user_id' => Auth::id(),
                    ]);
        }else if($request->meta == null && $request->image_bg != null){
            $image_bg = time().'.'.$request->image_bg->getClientOriginalExtension();
            $request->image_bg->move(public_path('uploads/events'), $image_bg);
            Event::where('event_id', $id)
                ->update([
                    'title' => $request->title,
                    'description' => $request->description,
                    'details' => $request->details,
                    'venue' => $request->venue,
                    'user_id' => Auth::id(),
                    'image_bg' => $image_bg,
                    ]);
        }else if($request->meta != null && $request->image_bg == null){
            $metaName = time().'.'.$request->meta->getClientOriginalExtension();
            $request->meta->move(public_path('uploads/events'), $metaName);
            Event::where('event_id', $id)
                ->update([
                    'title' => $request->title,
                    'description' => $request->description,
                    'details' => $request->details,
                    'venue' => $request->venue,
                    'user_id' => Auth::id(),
                    'meta' => $metaName,
                    ]);
        }else{
            $metaName = time().'.'.$request->meta->getClientOriginalExtension();
            $image_bg = time().'.'.$request->image_bg->getClientOriginalExtension();
            $request->meta->move(public_path('uploads/events'), $metaName);
            $request->image_bg->move(public_path('uploads/events'), $metaName);
            Event::where('event_id', $id)
                ->update([
                    'title' => $request->title,
                    'description' => $request->description,
                    'details' => $request->details,
                    'venue' => $request->venue,
                    'user_id' => Auth::id(),
                    'meta' => $metaName,
                    'image_bg' => $image_bg
                    ]);
        }
        return redirect('/admin/events');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        //
    }

    public function getEventDetail($id){
        $event = DB::table('events')->where('event_id', '=', $id)->get();
        return response()->json([
            'event_id' => $event[0]->event_id,
            'event_name' => $event[0]->title,
            'event_detail'=> $event[0]->details,
            'event_description'=> $event[0]->description,
            'event_pic' => $event[0]->meta,
            'event_venue' => $event[0]->venue,
            'event_time' => $event[0]->time,
            'event_date' => $event[0]->date,
        ]);
    }

    public function approveEvent($id){
        $user = Auth::user();
        if($user->user_type == "ADMIN"){
            try{
                $temp_event = Event::where('event_id',$id)
                            ->update(['status' => 'approved']);
                return response()->json([
                    'flag' => true,
                    'message' => 'Event has been approved successfully'
                ]);
            }
            catch (Exception $e) {
                return response()->json([
                    'flag' => false,
                    'message' => 'Sorry an error occured'
                ]);   
            }
        }
        else {
            return response()->json([
                    'flag' => false,
                    'message' => 'You do not have sufficient privilage'
                ]);   
        }
    }

    public function unapproveEvent($id){
        $user = Auth::user();
        if($user->user_type == "ADMIN"){
            try{
                $temp_event = Event::where('event_id',$id)
                            ->update(['status' => 'unapproved']);
                return response()->json([
                    'flag' => true,
                    'message' => 'Event has been unapproved successfully you can approve it again whenever you want'
                ]);
            }
            catch (Exception $e) {
                return response()->json([
                    'flag' => false,
                    'message' => 'Sorry an error occured'
                ]);   
            }
        }
        else {
            return response()->json([
                    'flag' => false,
                    'message' => 'You do not have sufficient privilage'
                ]);   
        }
    }

    public function getEventsList(Request $request){
        $events = DB::table('events')->where(
                'status','=','approved'
            )->get();
        if($events->count() != 0){
            $temp_events = $events->all();
            $events = array();
            $temp = array();
            foreach($temp_events as $event){
                $temp = array(
                    'title' => $event->title,
                    'description' => $event->description,
                    'date' => $event->date,
                    'time' => $event->time,
                    'venue' => $event->venue,
                    'meta' => $request->getSchemeAndHttpHost().'/uploads/events/'.$event->meta,
                    'bg_image' => $request->getSchemeAndHttpHost().'/uploads/events/'.$event->image_bg
                );
                array_push($events,$temp);
            }
            $data = array(
                'success' => true,
                'message' => "Success",
                'schedule' => $request->getSchemeAndHttpHost().'/storage/'.setting('schedule'),
                'events' => $events,
            );
            return $data;
        }else{
            $data = array(
                'flag' => false,
                'message' => "Coming Soon",
            );
            return $data;
        }
    }
}
