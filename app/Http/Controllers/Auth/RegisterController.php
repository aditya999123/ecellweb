<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/verify';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {   
        $otp = rand(999,10000);
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://sokt.io/K3tZvgczUjTii3xC9bzU/personal-e-cell",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\"otp\":\"".$otp."\",\"contact_no\":\"".$data['contact_no']."\",\"name\":\"".$data['name']."\"}",
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTPHEADER => array(
            "authkey: JAuwLD2HcJZnu9jkyNiK",
            "content-type: application/json"
            ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {                      
            Log::error("cURL Error #:" . $err);            
        } else {
            Log::info($response);            
        }      
        return User::updateOrCreate(
            [
                'email' => $data['email'],
                'contact_no' => $data['contact_no'],
            ],
            [
                'name' => $data['name'],
                'password' => bcrypt($data['password']),
                'roll_no' => $data['roll_no'],
                'institute' => $data['institute'],
                'semester' => $data['semester'],
                'branch' => $data['branch'],
                'otp' => $otp
            ]
        );
    }
}
