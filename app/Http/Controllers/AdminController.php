<?php

namespace App\Http\Controllers;
use Auth;
use Excel;
use App\Answer;
use App\Event;
use App\Sponsor;
use App\Startup;
use App\User;
use App\Speaker;
use App\QuestionSet;
use App\Question;
use App\Mentor;
use App\TeamMember;
use App\ContactUs;
use App\AppVersion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use sendotp\sendotp;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Keychain; // just to make our life simpler
// use Lcobucci\JWT\Signer\Ecdsa\Sha256; // you can use Lcobucci\JWT\Signer\Ecdsa\Sha256 if you're using ECDSA keys

class AdminController extends Controller
{

	/*
		Middleware Auth so that if a user is not logged it must be
		thrown away from here.
	*/


	public function __construct(){
        $this->middleware('auth',['except'=>['createUser', 'verify_otp']]);
	}


    public function index(){
        $user = Auth::user();
    	if($user->user_type == "ADMIN"){
            Log::info('Showing Admin Panel for user: '.$user->id);
    		$data = array(
	    		'users' => DB::table('users')->count(),
	        	'events' => DB::table('events')->count(),
	        	'sponsors' => DB::table('sponsors')->count(),
	        	'startups' => DB::table('startups')->count(),
                'speakers' => DB::table('speakers')->count(),
                'questionSets' => QuestionSet::all(),
        	);
    		return view('admin.index',$data);
    	}
    	else{
            Log::info('Unauthorised Access Try by the user : '.$user->id);
    		return redirect()->route('home');
    	}
    }

    public function events (){
        $user = Auth::user();
        if($user->user_type == "ADMIN"){
            Log::info('Showing Events section in Admin Panel for user: '.$user->id);
            $data = Event::all();
            return view('admin.events.index', ['events' => $data,'questionSets' => QuestionSet::all()]);
        }else{
            return redirect()->route('home');
        }
    }

    public function sponsors(){
        $user = Auth::user();
        if($user->user_type == "ADMIN"){
            Log::info('Showing Sponsors section in Admin Panel for user: '.$user->id);
            $data = Sponsor::all();
            return view('admin.sponsors.index', ['sponsors' => $data,'questionSets' => QuestionSet::all()]);
        }else{
            return redirect()->route('home');
        }
    }

    public function startups(){
        $user = Auth::user();
        if($user->user_type == "ADMIN"){
            Log::info('Showing Startups section in Admin Panel for user: '.$user->id);
            $data = Startup::all();
            return view('admin.startups.index', ['startups' => $data,'questionSets' => QuestionSet::all()]);
        }else{
            return redirect()->route('home');
        }
    }

    public function users(){
        $user = Auth::user();
        if($user->user_type == "ADMIN"){
            Log::info('Showing Users section in Admin Panel for user: '.$user->id);
            $data = User::all();
            return view('admin.users.index', ['users' => $data,'questionSets' => QuestionSet::all()]);
        }else{
            return redirect()->route('home');
        }       
    }

    public function speakers(){
        $user = Auth::user();
        if($user->user_type == "ADMIN"){
            Log::info('Showing Speakers section in Admin Panel for user: '.$user->id);
            $data = Speaker::all();
            return view('admin.speakers.index',['speakers' => $data,'questionSets' => QuestionSet::all()]);
        }else{
            return redirect()->route('home');
        }     
    }

    public function questionSets(){
        $user = Auth::user();
        if($user->user_type == "ADMIN"){
            Log::info('Showing QuestionSets section in Admin Panel for user: '.$user->id);
            $data = QuestionSet::all();
            return view('admin.questionSets.index',['questionSets' => $data,'questionSets' => QuestionSet::all()]);
        }else{
            return redirect()->route('home');
        }
    }

    public function questions(){
        $user = Auth::user();
        if($user->user_type == "ADMIN"){
            Log::info('Showing Questions section in Admin Panel for user: '.$user->id);
            $questionSets = QuestionSet::all();
            $questions = Question::all();
            return view('admin.questions.index',['questions' => $questions,'questionSets' => $questionSets]);
        }else{
            return redirect()->route('home');
        }
    }

    public function mentors(){
        $user = Auth::user();
        if($user->user_type == "ADMIN"){
            Log::info('Showing Mentors section in Admin Panel for user: '.$user->id);
            $mentors = Mentor::all();
            return view('admin.mentors.index',['mentors' => $mentors]);
        }else{
            return redirect()->route('home');
        }
    }

    public function teamMembers(){
        $user = Auth::user();
        if($user->user_type == "ADMIN"){
            Log::info('Showing TeamMembers section in Admin Panel for user: '.$user->id);
            $members = TeamMember::all();
            return view('admin.team_members.index',['team_members' => $members]);
        }else{
            return redirect()->route('home');
        }
    }

    public function addEvent(){
        $user = Auth::user();
        if($user->user_type == "ADMIN"){
            Log::info('Showing Events add section in Admin Panel for user: '.$user->id);
            return view('admin.events.add');
        }else{
            return redirect()->route('home');
        }
    }

    public function editEvent($id){
        $user = Auth::user();
        if($user->user_type == "ADMIN"){
            $event = Event::where('event_id',$id)->first();
            return view('admin.events.edit',['event' => $event]);
        }else{
            return redirect()->route('home');
        }
    }

    public function contactUs(){
        $user = Auth::user();
        if($user->user_type == "ADMIN"){
            Log::info('Showing Contactus section in Admin Panel for user: '.$user->id);
            $messages = ContactUs::all();
            return view('admin.messages.index',['messages' => $messages]);
        }else{
            return redirest()->route('home');
        }
    }

    // Create user
    public function createUser(Request $request){
        try{
            Log::info('Creating user');
            $count = User::where('contact_no', $request->contact_no)->count();
            // var_dump($request);
            // return $request;
            if($count == 0){
                // $keychain = new Keychain();
                Log::info('User Not Found '.$request->contact_no);
                $user = new User;
                $user->name = $request->name;
                $user->email = $request->email;
                $user->contact_no = $request->contact_no;
                $otp = rand(999,10000);
                $user->password = bcrypt($otp);
                $user->otp = $otp;            
                if($user->save()){
                    // $signer = new Sha256();
                    Log::info('User created successfully '.$request->contact_no);
                    $token = (new Builder())->setIssuer('https://ecell.nitrr.ac.in') // Configures the issuer (iss claim)                    
                            ->setIssuedAt(time()) // Configures the time that the token was issue (iat claim)
                            ->setId('4f1g23a12aa', true) // Configures the id (jti claim), replicating as a header item                    
                            // ->set('user_id', $user->id) // Configures a new claim, called "uid"
                            ->set('contact_no',$request->contact_no)
                            ->set('email',$request->email)
                            ->set('name',$request->name)
                            // ->sign($signer, 'Testing')
                            ->getToken(); // Retrieves the generated token
                    
                    $ch = curl_init();
                    Log::info($otp);
                    Log::info($request->contact_no);
                    Log::info($request->name);
                    //Your authentication key
                        $authKey = "125195AnE7snTWFepK5925ea7c";

                        //Multiple mobiles numbers separated by comma
                        $mobileNumber = $request->contact_no;

                        //Sender ID,While using route4 sender id should be 6 characters long.
                        $senderId = "ECellR";

                        //Your message to send, Add URL encoding here.
                        $message = urlencode("Your OTP for E-Cell NITRR is ".$otp."  . Please keep it confidential. Visit our website https://ecell.nitrr.ac.in for more.");

                        //Define route 
                        $route = "4";
                        //Prepare you post parameters
                        $postData = array(
                        'authkey' => $authKey,
                        'mobiles' => $mobileNumber,
                        'message' => $message,
                        'sender' => $senderId,
                        'route' => $route
                        );

                        //API URL
                        $url="https://control.msg91.com/api/sendhttp.php";

                        // init the resource
                        $ch = curl_init();
                        Log::info($url);
                        Log::info($postData);
                        curl_setopt_array($ch, array(
                            CURLOPT_URL => $url,
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_POST => true,
                            CURLOPT_POSTFIELDS => $postData
                        //,CURLOPT_FOLLOWLOCATION => true
                        ));


                        //Ignore SSL certificate verification
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


                        //get response
                        $output = curl_exec($ch);

                        //Print error if any

                        curl_close($ch);

                        Log::info($output);
                    // curl_setopt_array($curl, array(
                    //     CURLOPT_URL => "https://sokt.io/K3tZvgczUjTii3xC9bzU/personal-e-cell",
                    //     CURLOPT_RETURNTRANSFER => true,
                    //     CURLOPT_ENCODING => "",
                    //     CURLOPT_MAXREDIRS => 10,
                    //     CURLOPT_TIMEOUT => 30,
                    //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    //     CURLOPT_CUSTOMREQUEST => "POST",
                    //     CURLOPT_POSTFIELDS => "{\"otp\":\"".$otp."\",\"contact_no\":\"".$request->contact_no."\",\"name\":\"".$request->name."\"}",
                    //     CURLOPT_SSL_VERIFYHOST => 0,
                    //     CURLOPT_SSL_VERIFYPEER => 0,
                    //     CURLOPT_HTTPHEADER => array(
                    //     "authkey: JAuwLD2HcJZnu9jkyNiK",
                    //     "content-type: application/json"
                    //     ),
                    // ));
                        
                    // $response = curl_exec($curl);
                    // $err = curl_error($curl);
                    
                    // curl_close($curl);
                    
                    // if (curl_errno($ch)) {                      
                    //     Log::error("cURL Error #:" . $ch);
                    //     $data = array(
                    //         'success' => false,
                    //         'message' => "Sorry! an error occured in server",
                    //     );
                    // } else {
                        // Log::info($response);
                        // $token = array(
                        //     'user_id' => $user->id,
                        //     'contact_no' => $request->contact_no,
                        //     'email' => $request->email,
                        //     'name' => $request->name
                        // );
                        // $token = JWT::encode($token, 'EcellSecret2k17');    
                        $data = array(
                            'success' => true,
                            'message' => "User created successfully",
                            'token' => (string)$token
                        );            
                    // }        
                }else{
                    Log::info('Cannot save user '.$request->contact_no);
                    $data = array(
                        'success' => false,
                        'message' => "Sorry! an error occured in server",
                    );
                }
            }else{
                // $keychain = new Keychain();
                Log::info('User Found and otp will be send again '.$request->contact_no);
                $otp = rand(999,10000);
                $user = User::where('contact_no', '=' ,$request->contact_no)
                    ->update([
                        'otp' => $otp,
                        'email' => $request->email,
                        'name' => $request->name,
                    ]);
                if($user == 1){
                    Log::info('User updated successfully '.$request->contact_no);
                    // $user = User::where('contact_no',$request->contact_no);                
                    $token = (new Builder())->setIssuer('https://ecell.nitrr.ac.in') // Configures the issuer (iss claim)                    
                        ->setIssuedAt(time()) // Configures the time that the token was issue (iat claim)
                        ->setId('4f1g23a12aa', true) // Configures the id (jti claim), replicating as a header item                    
                        // ->set('user_id', $user->id) // Configures a new claim, called "user_id"
                        ->set('contact_no',$request->contact_no)
                        ->set('email',$request->email)
                        ->set('name',$request->name)
                        // ->sign($signer, 'Testing')
                        ->getToken(); // Retrieves the generated token

                    $ch = curl_init();
                    Log::info($otp);
                    Log::info($request->contact_no);
                    Log::info($request->name);
                    //Your authentication key
                        $authKey = "125195AnE7snTWFepK5925ea7c";

                        //Multiple mobiles numbers separated by comma
                        $mobileNumber = $request->contact_no;

                        //Sender ID,While using route4 sender id should be 6 characters long.
                        $senderId = "ECellR";

                        //Your message to send, Add URL encoding here.
                        $message = urlencode("Your OTP for E-Cell NITRR is ".$otp."  . Please keep it confidential. Visit our website https://ecell.nitrr.ac.in for more.");

                        //Define route 
                        $route = "4";
                        //Prepare you post parameters
                        $postData = array(
                        'authkey' => $authKey,
                        'mobiles' => $mobileNumber,
                        'message' => $message,
                        'sender' => $senderId,
                        'route' => $route
                        );

                        //API URL
                        $url="https://control.msg91.com/api/sendhttp.php";

                        // init the resource
                        $ch = curl_init();
                        Log::info($url);
                        Log::info($postData);
                        curl_setopt_array($ch, array(
                            CURLOPT_URL => $url,
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_POST => true,
                            CURLOPT_POSTFIELDS => $postData
                        //,CURLOPT_FOLLOWLOCATION => true
                        ));


                        //Ignore SSL certificate verification
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


                        //get response
                        $output = curl_exec($ch);

                        //Print error if any

                        curl_close($ch);

                        Log::info($output);

                        $data = array(
                            'success' => true,
                            'message' => "User updated successfully",
                            'token' => (string)$token
                        ); 
                    // $curl = curl_init();
                        
                    // curl_setopt_array($curl, array(
                    //     CURLOPT_URL => "https://sokt.io/K3tZvgczUjTii3xC9bzU/personal-e-cell",
                    //     CURLOPT_RETURNTRANSFER => true,
                    //     CURLOPT_ENCODING => "",
                    //     CURLOPT_MAXREDIRS => 10,
                    //     CURLOPT_TIMEOUT => 30,
                    //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    //     CURLOPT_CUSTOMREQUEST => "POST",
                    //     CURLOPT_POSTFIELDS => "{\"otp\":\"".$otp."\",\"contact_no\":\"".$request->contact_no."\",\"name\":\"".$request->name."\"}",
                    //     CURLOPT_SSL_VERIFYHOST => 0,
                    //     CURLOPT_SSL_VERIFYPEER => 0,
                    //     CURLOPT_HTTPHEADER => array(
                    //     "authkey: JAuwLD2HcJZnu9jkyNiK",
                    //     "content-type: application/json"
                    //     ),
                    // ));
                        
                    // // $response = curl_exec($curl);
                    // $err = curl_error($curl);
                    
                    // curl_close($curl);
                    
                    // if ($err) {                      
                    //     Log::error("cURL Error #:" . $err);
                    //     $data = array(
                    //         'success' => false,
                    //         'message' => "Sorry! an error occured in server",
                    //     );
                    // } else {
                    //     // Log::info($response);
                    //     $data = array(
                    //         'success' => true,
                    //         'message' => "User updated successfully",
                    //         'token' => (string)$token
                    //     );            
                    // }


                }else{
                    Log::info('Cannot save user '.$request->contact_no);
                    $data = array(
                        'success' => false,
                        'message' => "Sorry! an error occured in server",
                    );
                }
            }
        }
        catch(Exception $e){
            Log::error($e->getMessage());
        //     $otp = rand(999,10000);
        //     $token = (new Builder())->setIssuer('https://ecell.nitrr.ac.in') // Configures the issuer (iss claim)                    
        //             ->setIssuedAt(time()) // Configures the time that the token was issue (iat claim)
        //             ->setId('4f1g23a12aa', true) // Configures the id (jti claim), replicating as a header item                    
        //             // ->set('user_id', $user->id) // Configures a new claim, called "user_id"
        //             ->set('contact_no',$request->contact_no)
        //             ->set('email',$request->email)
        //             ->set('name',$request->name)
        //             // ->sign($signer, 'Testing')
        //             ->getToken(); // Retrieves the generated token
        
        //     $curl = curl_init();
                
        //     curl_setopt_array($curl, array(
        //         CURLOPT_URL => "https://sokt.io/K3tZvgczUjTii3xC9bzU/personal-e-cell",
        //         CURLOPT_RETURNTRANSFER => true,
        //         CURLOPT_ENCODING => "",
        //         CURLOPT_MAXREDIRS => 10,
        //         CURLOPT_TIMEOUT => 30,
        //         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //         CURLOPT_CUSTOMREQUEST => "POST",
        //         CURLOPT_POSTFIELDS => "{\"otp\":\"".$otp."\",\"contact_no\":\"".$request->contact_no."\",\"name\":\"".$request->name."\"}",
        //         CURLOPT_SSL_VERIFYHOST => 0,
        //         CURLOPT_SSL_VERIFYPEER => 0,
        //         CURLOPT_HTTPHEADER => array(
        //         "authkey: JAuwLD2HcJZnu9jkyNiK",
        //         "content-type: application/json"
        //         ),
        //     ));
                
        //     $response = curl_exec($curl);
        //     $err = curl_error($curl);
            
        //     curl_close($curl);
            
        //     if ($err) {                      
        //         Log::error("cURL Error #:" . $err);
        //         $data = array(
        //             'success' => false,
        //             'message' => "Sorry! an error occured in server",
        //         );
        //     } else {
        //         Log::info($response);
        //         $data = array(
        //             'success' => true,
        //             'message' => "User updated successfully",
        //             'token' => (string)$token
        //         );            
        //     }        
        //     $user = User::where('email', '=' ,$request->email)
        //         ->update([
        //             'otp' => $otp,
        //             'contact_no' => $request->contact_no,
        //             'name' => $request->name,
        //         ]);
        //     Log::error('The only threat left is email and contact_no exixts');
        //     $data = array(
        //         'success' => true,
        //         'message' => 'Email and contact no combination already exixts',
        //         'token' => (string)$token
        //     );
        // }
            $data = array(
                'success' => false,
                'message' => 'Sorry email already exists'
            );
        }
        return $data;   
    }

    public function verify_otp(Request $request){
        Log::info('Verifying user otp '.$request->otp);
        Log::info('Verifying user token '.$request->token);
        Log::info('User contact number '.$request->mobile);
        $token = (new Parser())->parse((string) $request->token);
        $data = $token->getClaims();
        $sent_otp = $request->otp;
        $user = User::where('contact_no',$data['contact_no'])->get();
        if($user[0]->otp == $sent_otp){
            User::where('contact_no', '=' ,$data['contact_no'])
            ->update([
                'status' => 1,
            ]);
            Log::info('User OTP verified '.$token);
            $data = array(
                'success' => true,
                'message' => 'Your OTP has been verified successfully'
            );
        }else{
            Log::info('User entered wrong OTP'.$token);
            $data = array(
                'success' => false,
                'message' => 'Wrong OTP'
            );
        }
        return $data;
    }

    public function addVersion(Request $request){
        if(Auth::user()->user_type == "ADMIN"){
            $app_version = new AppVersion;
            Log::info('Adding new version: '.Auth::user()->id);
            $app_version->version = $request->version;
            $app_version->change_log = $request->change_log;
            $app_version->url = $request->url;
            if($app_version->save()){
                return redirect('/admin/show_app_versions');
            }else{
                return redirect('/admin/show_app_versions');
            }
        }else{
            return redirect()->route('home');
        }
        
    }

    public function appVersions(Request $request){
        $user = Auth::user();
        if($user->user_type == "ADMIN"){
            Log::info('Showing app versions : '.$user->id);
            $versions = AppVersion::all();
            return view('admin.app_versions.index',['versions' => $versions]);
        }else{
            return redirect()->route('home');
        }
    }

    public function sendNotifications(){
        $user = Auth::user();
        if($user->user_type == "ADMIN"){
            $curl = curl_init();
                    
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://sokt.io/K3tZvgczUjTii3xC9bzU/personal-e-cell",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => "{\"otp\":\"".$otp."\",\"contact_no\":\"".$request->contact_no."\",\"name\":\"".$request->name."\"}",
                    CURLOPT_SSL_VERIFYHOST => 0,
                    CURLOPT_SSL_VERIFYPEER => 0,
                    CURLOPT_HTTPHEADER => array(
                    "authkey: JAuwLD2HcJZnu9jkyNiK",
                    "content-type: application/json"
                    ),
                ));
                    
                // $response = curl_exec($curl);
                $err = curl_error($curl);
                
                curl_close($curl);
                
                if ($err) {                      
                    Log::error("cURL Error #:" . $err);
                    $data = array(
                        'success' => false,
                        'message' => "Sorry! an error occured in server",
                    );
                }
        }
    }

    public function exportToExcel(){
        try{
            $user = Auth::user();
            if($user->user_type == 'ADMIN'){
                $users = User::select('id','contact_no', 'name')
                        ->get();
                // Initialize the array which will be passed into the Excel
                // generator.
                $usersArray = []; 
                // Define the Excel spreadsheet headers
                $usersArray[] = ['id', 'contact_no','name'];
                // Convert each member of the returned collection into an array,
                // and append it to the payments array.
                foreach ($users as $user) {
                    $usersArray[] = $user->toArray();
                }
                // Generate and return the spreadsheet
                Excel::create('users', function($excel) use ($usersArray) {
            
                    // Set the spreadsheet title, creator, and description
                    $excel->setTitle('Users');
                    $excel->setCreator('Entrepreneurship Cell')->setCompany('NIT Raipur');
                    $excel->setDescription('User Details');
            
                    // Build the spreadsheet, passing in the payments array
                    $excel->sheet('sheet1', function($sheet) use ($usersArray) {
                        $sheet->fromArray($usersArray, null, 'A1', false, false);
                    });
            
                })->download('xlsx');
            }
        }catch(\Exception $e){
            Log::error($e->getMessage());
        }
    }

    public function showAnswers(){
        $user = Auth::user();
        if($user->user_type == 'ADMIN'){
            Log::info('Showing answers section in Admin Panel for user: '.$user->id);
            $data = Answer::all();
            return view('admin.answers.index',['answers' => $data]);
        }else{
            return redirect()->route('home');
        }
    }

    public function approveAnswer($id){
        $user = Auth::user();
        if($user->user_type == 'ADMIN'){
            $count = Answer::where('id',$id)
                            ->update(['status' => 'approved']);
            Log::info('Answer approved by user_id '.$user->id.' answer_id '.$id);
            $data = array(
                'success' => true,
                'message' => 'Approved successfully'
            );
        }else{
            $data = array(
                'success' => false,
                'message' => 'Not sufficient privilage'
            );
        }
        return $data;
    }

    public function unapproveAnswer($id){
        $user = Auth::user();
        if($user->user_type == 'ADMIN'){
            $count = Answer::where('id',$id)
                            ->update(['status' => 'unapproved']);
            Log::info('Answer approved by user_id '.$user->id.' answer_id '.$id);
            $data = array(
                'success' => true,
                'message' => 'Unapproved successfully'
            );
        }else{
            $data = array(
                'success' => false,
                'message' => 'Not sufficient privilage'
            );
        }
        return $data;
    }

    public function showLeaderboard(){
        $user = Auth::user();
        if($user->user_type == 'ADMIN'){
            $leaders = array();
            $users = DB::table('answers')
                    ->select('user_id')
                    ->distinct()
                    // ->orderBy('updated_at', 'asc')
                    ->get();
            foreach($users as $user){
                $count = DB::table('answers')
                        ->where('status','=','approved')
                        ->where('user_id','=',$user->user_id)
                        ->count();
                $temp = array(
                    'username' => DB::table('users')->select('name','contact_no')->where('id','=',$user->user_id)->get(),
                    'marks' => $count*100,
                );
                array_push($leaders,$temp);
            }
            return view('admin.leaderboard',['users' => $leaders]);
        }else{
            return redirect()->route('home');
        }
    }

    public function sendBulkMessage(Request $request){
        $user = Auth::user();
        if($user->user_type == 'ADMIN'){
            $users = DB::table('users')
                    ->select('contact_no')
                    ->distinct()
                    ->get();
            $temp = array();
            foreach($users as $user){
                $user = $user->contact_no;
                array_push($temp, $user);
            }
            $curl = curl_init();
            $post_data = array(
                'contact_no' => $temp,
                'message' => $request->message
            );
            $post_data = json_encode($post_data);
            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://sokt.io/kEpFqRKUakiWfsSPQHCM/personal-e-cell-bulk",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => $post_data,
              CURLOPT_SSL_VERIFYHOST => 0,
              CURLOPT_SSL_VERIFYPEER => 0,
              CURLOPT_HTTPHEADER => array(
                "authkey: JAuwLD2HcJZnu9jkyNiK",
                "content-type: application/json"
              ),
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            
            curl_close($curl);
            
            if ($err) {
                Log::error("cURL Error #:" . $err);
                $data = array(
                    'success' => false,
                    'message' => 'Sorry an error occured'
                );
            } else {
                log::info($response);
                $data = array(
                    'success' => true,
                    'message' => 'Message sent successfully'
                );
            }
            return $data;
        }else{
            return redirect()->route('home');
        }
    }
}