<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\AppVersion;

class AppVersionController extends Controller
{
    public function __construct(){
        $this->middleware('auth',['except' => ['isUpdateAvailable']]);
    }
    
    public function approveVersion($id){
        $user = Auth::user();
        if($user->user_type == "ADMIN"){
            try{
                $temp_event = AppVersion::where('id',$id)
                            ->update(['status' => 'approved']);
                return response()->json([
                    'flag' => true,
                    'message' => 'App Version has been approved successfully'
                ]);
            }
            catch (Exception $e) {
                return response()->json([
                    'flag' => false,
                    'message' => 'Sorry an error occured'
                ]);   
            }
        }
        else {
            return response()->json([
                    'flag' => false,
                    'message' => 'You do not have sufficient privilage'
                ]);   
        }
    }

    public function unapproveVersion($id){
        $user = Auth::user();
        if($user->user_type == "ADMIN"){
            try{
                $temp_event = AppVersion::where('id',$id)
                            ->update(['status' => 'unapproved']);
                return response()->json([
                    'flag' => true,
                    'message' => 'App Version has been unapproved successfully'
                ]);
            }
            catch (Exception $e) {
                return response()->json([
                    'flag' => false,
                    'message' => 'Sorry an error occured'
                ]);   
            }
        }
        else {
            return response()->json([
                    'flag' => false,
                    'message' => 'You do not have sufficient privilage'
                ]);   
        }
    }

    public function isUpdateAvailable(){
        $count = AppVersion::where('status','approved')->count();
        if($count > 0){
            $version = AppVersion::where('status','approved')->get();
            $data = array(
                'success' => true,
                'message' => 'Updates are avialable',
                'version' => $version[0]->version
            );
        }else{
            $data = array(
                'success' => false,
                'message' => 'Sorry no updates are available'
            );
        }
        return $data;

    }
}
