<?php

namespace App\Http\Controllers;

use App\Speaker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Fcm;
use App\User;
use App\AppVersion;
// use AuthenticatesUsers;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => ['index','vision','team','blogs','bmodel','submitFcm','sendOtp','verifyOtp']]);
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        // $speakers = Speaker::where("status","approved");
        // return view('welcome',['speakers' => $speakers]);
        return view('new_index');
    }

    public function welcome()
    {   
        $users = DB::table('users')->count();
        $events = DB::table('events')->count();
        return view('home',['events'=>$events,'users'=>$users]);
    }

    public function vision()
    {
        return view('vision');
    }

    public function team()
    {
        return view('team');
    }

    public function blogs(){
        return view('blogs.index');
    }

    public function bmodel(){
        return view('bmodel');
    }

    public function profile(){
        $this->middleware('auth');
        return view('profile');
    }

    public function submitFcm(Request $request){
	Log::debug('Request Caught');
	$data = array(
		'success' => false,
		'message' => 'Sorry no updates are available'
	);
        return $data;
    }

    public function verify(){
        $user = Auth::user();
        if($user->status == false){
            return view('auth.verify');
        }else{
            return redirect('home');
        }
    }

    public function resendOtp(){
        $current_user = Auth::user();
        $contact_no = $current_user->contact_no;
        $name = $current_user->name;
        $user = User::find($current_user->id);
        $otp = rand(999,10000);
        $user->otp = $otp;
        if($user->save()){
            $curl = curl_init();
            
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://sokt.io/K3tZvgczUjTii3xC9bzU/personal-e-cell",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "{\"otp\":\"".$otp."\",\"contact_no\":\"".$contact_no."\",\"name\":\"".$name."\"}",
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_HTTPHEADER => array(
                "authkey: JAuwLD2HcJZnu9jkyNiK",
                "content-type: application/json"
                ),
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            
            curl_close($curl);
            
            if ($err) {                      
                Log::error("cURL Error #:" . $err);
                $data = array(
                    'success' => false,
                    'message' => 'Sorry unable to resend OTP Please try again after few minutes.'
                );
            } else {
                Log::info($response);
                $data = array(
                    'success' => true,
                    'message' => 'OTP has been send again.'
                );
            }
        }else{
            $data = array(
                'success' => false,
                'message' => 'Sorry unable to resend OTP Please try again after few minutes.'
            );
        }
        return $data;
    }

    public function sendOtp(Request $request){
        $otp = rand(999,10000);
        $user = DB::table('users')
                ->where('contact_no',$request->contact_no)
                ->update(['otp' => $otp]);
        if($user == 1){
            $curl = curl_init();
            $user = DB::table('users')
                    ->where('contact_no',$request->contact_no)
                    ->get();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://sokt.io/K3tZvgczUjTii3xC9bzU/personal-e-cell",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "{\"otp\":\"".$otp."\",\"contact_no\":\"".$request->contact_no."\",\"name\":\"".$user[0]->name."\"}",
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_HTTPHEADER => array(
                "authkey: JAuwLD2HcJZnu9jkyNiK",
                "content-type: application/json"
                ),
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            
            curl_close($curl);
            
            if ($err) {                      
                Log::error("cURL Error #:" . $err);
                $data = array(
                    'success' => false,
                    'message' => 'Sorry unable to resend OTP Please try again after few minutes.'
                );
            } else {
                Log::info($response);
                $data = array(
                    'success' => true,
                    'message' => 'OTP has been send again.'
                );
            }
        }else{
            $data = array(
                'success' => false,
                'message' => 'Sorry unable to resend OTP Please try again after few minutes.'
            );
        }
        return $data;
    }

    public function verifyOtp(Request $request){
        try{
            $current_user = Auth::user();
            $user = DB::table('users')
                    ->where('id',$current_user->id)
                    ->get();
            $user = $user[0];
            // $user = User::find($current_user->id)->get();
            if($user->otp == $request->otp){
                $user = DB::table('users')
                    ->where('id',$current_user->id)
                    ->update(['status' => true]);
                if($user == 1){
                    $data = array(
                        'success' => true,
                        'message' => 'OTP has been verified successfully'
                    );
                }else{
                    $data = array(
                        'success' => false,
                        'message' => 'Sorry an error occured'
                    );
                }
            }else{
                $data = array(
                    'success' => false,
                    'message' => "Sorry an error occured"
                );
            }
        }catch(\Exception $e){
            try{
                Log::info("User is not logged in which means user is calling api for login");
                $contact_no = $request->contact_no;
                $otp = $request->otp;
                Log::info($request->contact_no);
                $user_count = DB::table('users')
                            ->where('contact_no',$contact_no)
                            ->where('otp',$otp)
                            ->count();
                $user = DB::table('users')
                        ->where('contact_no',$contact_no)
                        ->get();
                Log::info('User Details '.$user[0]->id);
                if($user_count == 1){
                    if (Auth::loginUsingId($user[0]->id)) {
                        $data = array(
                            'success' => true,
                            'message' => 'OTP has been verified successfully'
                        );
                        Log::info("User Logged in ");
                    }else{
                        $data = array(
                            'success' => false,
                            'message' => 'Cannot log user'
                        );
                        Log::info("Cannot log user in");
                    }
                }else{
                    $data = array(
                        'success' => false,
                        'message' => 'OTP is not valid'
                    );
                    Log::info("User Not found");
                }
            }catch(\Exception $e){
                Log::info("Exception response ".$e->getMessage());
                $data = array(
                    'success' => false,
                    'message' => 'Sorry an error occured'
                );
            }
        }
        return $data;
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('welcome');
    }
}
