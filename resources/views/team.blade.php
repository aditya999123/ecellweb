@extends('layouts.app')
@section('title', 'Team')

@section('content')
<!-- Team -->
<link href="/css/team.css" rel="stylesheet">
<section id="team" style="padding:50px 15px;text-align:left;">
                <div class="text-center">
                    <h2>Team</h2>
                    <hr style="width:20%;">
                    <br>
                    <br>
                    <h3>Director NIT Raipur</h3>
                    <hr style="width:20%;">
                    <br>
                </div>
                <ul class="ch-grid">
                    <li>
                        <div class="ch-item">
                            <div class="ch-info">
                                <div class="ch-info-front ch-img-0"></div>
                                <div class="ch-info-back">
                                    <h3>Dr A M Rawani</h3>
                                    <p>Director, NIT Raipur
                                        <br><a href="http://www.nitrr.ac.in/director.php">View on NITRR Website</a></p>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="text-center">
                            <h3>Dean Student Welfare</h3>
                            <hr style="width:20%;">
                            <br>
                        </div>
                        <ul class="ch-grid">
                            <li>
                                <div class="ch-item">
                                    <div class="ch-info">
                                        <div class="ch-info-front ch-img-dean"></div>
                                        <div class="ch-info-back">
                                            <h3>Dr. Prabhat Diwan</h3>
                                            <p>Dean Student Welfare</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-4">
                        <div class="text-center">
                            <h3>Dean Research & Cons.</h3>
                            <hr style="width:20%;">
                            <br>
                        </div>
                        <ul class="ch-grid">
                            <li>
                                <div class="ch-item">
                                    <div class="ch-info">
                                        <div class="ch-info-front ch-img-deanrc"></div>
                                        <div class="ch-info-back">
                                            <h3>Dr. S. Sanyal</h3>
                                            <p>Dean Research & Consultancy</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-4">
                        <div class="text-center">
                            <h3>Faculty Incharge</h3>
                            <hr style="width:20%;">
                            <br>
                        </div>
                        <ul class="ch-grid">
                            <li>
                                <div class="ch-item">
                                    <div class="ch-info">
                                        <div class="ch-info-front ch-img-incharge"></div>
                                        <div class="ch-info-back">
                                            <h3>Dr. Subhas Ganguly</h3>
                                            <p>Faculty Incharge E-Cell NIT Raipur</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                
                <div class="text-center">
                    <h3>Overall Co-ordinators</h3>
                    <hr style="width:20%;">
                    <br>
                </div>
                <ul class="ch-grid" id="overall_co_ordinators">

                </ul>
                
                <div class="text-center">
                    <h3>Head Co-ordinators</h3>
                    <hr style="width:20%;">
                    <br>
                </div>
                <ul class="ch-grid" id="head_co_ordinators">

                </ul>

                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text-center">
                                <h3>Managers</h3>
                                <hr style="width:20%;">
                                <br>
                            </div>
                            <table id="teamlist">
                                <tbody id="managers">
                                                                    
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <div class="text-center">
                                <h3>Executives</h3>
                                <hr style="width:20%;">
                                <br>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <table id="teamlist" style="width: 80%;">
                                        <tbody id="executives">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
<script>
    $("#team_navbar").css({'color':'white'});

	//I sincerely apologize if the whole thing I did is wrong _/\_

	$.ajax({
        url:"/get_team_members_list",
        success: function(data){
            if (data.success == true) {
                var counter_overall = 0;
                var counter_head = 0;
                var counter_managers = 0;
                var counter_executive = 0;
                var overall = '';
                var head = '';
                var managers = '<tr><th>Name:</th></tr>';
                var executives = '<tr><th>Name:</th><th>Name:</th></tr><tr>';
                for(i in data.team_members){				
                    if (data.team_members[i]['position'] == "overall") {
                        counter_overall = counter_overall + 1;
                        //change the below part according to your html format you need to show
                        overall += "<li>";
                        overall += "<div class=\"ch-item\">";
                                        overall += "<div class=\"ch-info\">";
                                        overall += "<div class=\"ch-info-front\" style=\"background-image: url("+data.team_members[i]['meta']+")\"></div>";
                                        overall += "<div class=\"ch-info-back\">";
                                        overall += "<h3>"+data.team_members[i]['name']+"</h3>";
                                            overall += "<p>Head Co-ordinator <a href=\""+data.team_members[i]['linkedIn']+"\" target=\"_blank\">View on Linkedin</a></p>";
                                        overall += "</div></div></div></li>";
                        if(counter_overall % 4 == 0 ){
                            overall += "</ul><ul class=\"ch-grid\" id=\"overall_co-ordinators\">";
                        }
                    }else if (data.team_members[i]['position'] == "head") {
                        counter_head = counter_head + 1;
                        //replace headcoordinator with correct position name
                        // change this part as well according to your html format you need to show
                        head += "<li>";
                                    head += "<div class=\"ch-item\">";
                                        head += "<div class=\"ch-info\">";
                                        head += "<div class=\"ch-info-front\" style=\"background-image: url("+data.team_members[i]['meta']+")\"></div>";
                                        head += "<div class=\"ch-info-back\">";
                                            head += "<h3>"+data.team_members[i]['name']+"</h3>";
                                            head += "<p>Head Co-ordinator <a href=\""+data.team_members[i]['linkedIn']+"\" target=\"_blank\">View on Linkedin</a></p>";
                                        head += "</div></div></div></li>";
                        if(counter_head % 4 == 0 ){
                            head += "</ul><ul class=\"ch-grid\">";
                        }
                    }else if (data.team_members[i]['position'] == "managers") {
                        counter_managers = counter_managers + 1;
                        //replace headcoordinator with correct position name
                        // change this part as well according to your html format you need to show
                        managers += '<tr><td>'+ data.team_members[i]['name'] +'</td></tr>';
                    }else if (data.team_members[i]['position'] == "executive") {
                        counter_executive = counter_executive + 1;
                        executives += '<td>'+data.team_members[i]['name']+'</td>';
                        if(counter_executive % 2 == 0){
                            executives +="</tr><tr>";
                        }
                    }
                }

                //closing overall and headcoordinators variable
                overall+="</ul>";
                head+="</ul>";
                managers += "</tr>";
                executives += "</tr>";
                console.log(data);

                //display these in your element with id
                $("#overall_co_ordinators").html(overall);
                $("#head_co_ordinators").html(head);
                $("#managers").html(managers);
                $("#executives").html(executives);

                console.log(overall);
                console.log(head);
                console.log(managers);
                console.log(executives);
            }
        },
        error: function(){
            alert("Sorry an error occured");
        }
	});
</script>
@endsection