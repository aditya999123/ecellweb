@extends('layouts.admin_panel')
@section('title', 'Messages')

@section('content')
@parent
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Messages
        <small>Entrepreneurship Summit 2k17</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Contact Us</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Messages</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th> 
                  <th>Name</th>
                  <th>Email</th>
                  <th>Subject</th>
                  <th>Messages</th>
                  <th>Updated At</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($messages as $message)
                    <tr align="center">
                        <td>{{ $message->id }}</td>
                        <td>{{ $message->name }}</td>
                        <td>{{ $message->email }}</td>
                        <td>{{ $message->subject }}</td>
                        <td>{{ $message->message }}</td>
                        <td>{{ $message->updated_at }}</td>
                        <!-- <td><button type="button" class="btn btn-default" onclick="edit_message({{ $message->message_id }})"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button></td>
                        <td><button type="button" class="btn btn-default" onclick="delete_message({{ $message->message_id }})"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td> -->
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                <th>#</th> 
                <th>Name</th>
                <th>Email</th>
                <th>Subject</th>
                <th>Messages</th>
                <th>Updated At</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          

        </section>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
@endsection