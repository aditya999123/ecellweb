@extends('layouts.admin_panel')
@section('title', 'App Versions')

@section('content')
@parent
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        App Versions
        <small>Entrepreneurship Summit 2k17</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Contact Us</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">App Versions</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th> 
                  <th>Version</th>
                  <th>Change Log</th>
                  <th>URL</th>
                  <th>Status</th>
                  <th>Created At</th>
                  <th>Updated At</th>
                  <!-- <th>Edit</th> -->
                </tr>
                </thead>
                <tbody>
                @foreach ($versions as $version)
                    <tr align="center">
                        <td>{{ $version->id }}</td>
                        <td>{{ $version->version }}</td>
                        <td>{{ $version->change_log }}</td>
                        <td>{{ $version->url }}</td>
                        <td>
                            <h4 id="version_{{ $version->id }}">
                                @if ($version->status === 'unapproved')
                                    <span class="label label-danger">Unapproved</span>
                                @else
                                    <span class="label label-success">Approved</span>
                                @endif
                            </h4>
                            <br>
                            <button type="button" class="btn btn-default" onclick="approve_version({{ $version->id }})">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                            </button>
                            <button type="button" class="btn btn-default" onclick="unapprove_version({{ $version->id }})">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                            </button>
                        </td>
                        <td>{{ $version->created_at }}</td>
                        <td>{{ $version->updated_at }}</td>
                        <!-- <td><button type="button" class="btn btn-default" onclick="edit_version({{ $version->id }})"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button></td> -->
                        <!-- <td><button type="button" class="btn btn-default" onclick="delete_version({{ $version->id }})"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td> -->
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th> 
                  <th>Version</th>
                  <th>Change Log</th>
                  <th>URL</th>
                  <th>Status</th>
                  <th>Created At</th>
                  <th>Updated At</th>
                  <!-- <th>Edit</th> -->
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          

        </section>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $("#app_versions_navbar").addClass("active");
    function edit_question(version_id){
        //Date picker
        $('#edit_question_date').datepicker({
            autoclose: true
        })
        $("#preloader").show();
        var version_id, question_name, question_detail, question_description, question_pic;    
        $.ajax({
            url: '/questions/get_question_detail/'+version_id,
            dataTyps: 'json',
            success: function(data){
                $("#preloader").hide();
                $("#edit_version_id").val(data.version_id);
                $("#edit_question_title").val(data.question_name);
                $("#edit_question_description").val(data.question_description);
                $("#edit_question_detail").val(data.question_detail);
                $("#edit_question_venue").val(data.question_venue);
                $("#edit_question_time").val(data.question_time);
                $("#edit_question_date").val(data.question_date);
                $("#edit_question_meta").val(data.question_pic);
                $("#edit_question_form").attr('action',"/questions/"+data.version_id);
                $("#edit_question_modal").modal('show');
            },
            error: function(){
                alert("Seems like you are not connected to the internet");
                $("#preloader").hide();
            }
        });_question
    }
    function delete_question(version_id){
        alert(version_id);
    }
    function approve_version(version_id){
        $("#preloader").show();
        $.ajax({
            url: '/app_versions/approve_version/'+version_id,
            dataType: 'json',
            success: function(data){
                if(data.flag == true){
                    $("#message_call").text(data.message);
                    $("#version_"+version_id).html("<span class=\"label label-success\">Approved</span>");
                }else{
                    $("#message_call").text(data.message);
                }
                $("#preloader").hide();
                $("#message_modal").modal('show');
                // update_table();
            },
            error: function(data){
                $("#message_call").text(data.message);
                $("#preloader").hide();
                $("#message_modal").modal('show');
            }
        });
    }
    function unapprove_version(version_id){
        $("#preloader").show();
        $.ajax({
            url: '/app_versions/unapprove_version/'+version_id,
            dataType: 'json',
            success: function(data){
                if(data.flag == true){
                    $("#message_call").text(data.message);
                    $("#version_"+version_id).html("<span class=\"label label-danger\">Unapproved</span>");
                }else{
                    $("#message_call").text(data.message);
                }
                $("#preloader").hide();
                $("#message_modal").modal('show');
                // update_table();
            },
            error: function(data){
                $("#message_call").text(data.message);
                $("#preloader").hide();
                $("#message_modal").modal('show');
            }
        });
    }
    // function update_table(){
    //   $.ajax({
    //     url: '/admin/get_questions',
    //     success: function(data){
    //       if(data.success == true){
    //         var questions = data.questions;
    //         for(i in questions){
    //           if(questions[i]['status'] == 'approved'){
    //             $("#version_"+questions[i]['id']).html("<span class=\"label label-success\">Approved</span>");
    //           }else{
    //             $("#version_"+questions[i]['id']).html("<span class=\"label label-danger\">Unapproved</span>");
    //           }
    //         }
    //         console.log(questions);
    //       }
    //     },
    //     error:function(){
    //       alert('It seems like you are not conected to the internet');
    //     }
    //   });
    // }
</script>
@endsection