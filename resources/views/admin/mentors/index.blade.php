@extends('layouts.admin_panel')
@section('title', 'Admin Panel')

@section('content')
@parent
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        mentors
        <small>Entrepreneurship Summit 2k17</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">mentors</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">mentors Detail</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th> 
                  <th>mentor Pic</th>
                  <th>mentor Name</th>
                  <th>Status</th>
                  <th>Description</th>
                  <th>Details</th>
                  <th>Contact No</th>
                  <th>Contact Email</th>
                  <th>Modified At</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($mentors as $mentor)
                    <tr align="center">
                        <td>{{ $mentor->mentor_id }}</td>
                        <td>
                            @if($mentor->meta == '')
                                <img src="/images/esummit_black.png" alt="E-Summit" class="img-rounded" width="75px">
                            @else
                                <img src="/uploads/mentors/{{ $mentor->meta }}" alt="E-Summit" class="img-rounded" height="75px" >
                            @endif
                        </td>
                        <td>{{ $mentor->name }}</td>
                        <td>
                            <h4 id="mentor_{{ $mentor->mentor_id }}">
                            @if ($mentor->status === 'unapproved')
                                <span class="label label-danger">Unapproved</span>
                            @else
                                <span class="label label-success">Approved</span>
                            @endif
                            </h4>
                            <br>
                            <button type="button" class="btn btn-default" onclick="approve_mentor({{ $mentor->mentor_id }})">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                            </button>
                            <button type="button" class="btn btn-default" onclick="unapprove_mentor({{ $mentor->mentor_id }})">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                            </button>
                        </td>
                        <td>{{ $mentor->description }}</td>
                        <td>{{ $mentor->details }}</td>
                        <td>{{ $mentor->contact_no }}</td>
                        <td>{{ $mentor->contact_email }}</td>
                        <td>{{ $mentor->updated_at }}</td>
                        <td><button type="button" class="btn btn-default" onclick="edit_mentor({{ $mentor->mentor_id }})"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button></td>
                        <td><button type="button" class="btn btn-default" onclick="delete_mentor({{ $mentor->mentor_id }})"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>#</th> 
                    <th>mentor Pic</th>
                    <th>mentor Name</th>
                    <th>Status</th>
                    <th>Description</th>
                    <th>Details</th>
                    <th>Contact No</th>
                    <th>Contact Email</th>
                    <th>Modified At</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          

        </section>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
<div class="modal fade" tabindex="-1" role="dialog" id="edit_mentor_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form role="form" enctype="multipart/form-data" method="POST" action="/mentors/update_mentor" id="edit_mentor_form">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <input type="hidden" name="mentor_id">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel">Edit mentor</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="input-group">
                                <span class="input-group-addon">Name</span>
                                <input type="text" class="form-control" placeholder="mentor Title" name="name" id="edit_mentor_title">
                            </div>
                        </div>
                    </div><br>
                    <div class="row">
                      <div class="col-lg-12">
                            <div class="input-group">
                                <span class="input-group-addon">Description</span>
                                <textarea type="text" class="form-control" placeholder="mentor Description" name="description" id="edit_mentor_description"></textarea>
                            </div>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="input-group">
                                <span class="input-group-addon">Detail</span>
                                <textarea placeholder="mentor Details" class="form-control" rows="5" name="details" id="edit_mentor_detail"></textarea>
                            </div>
                        </div>
                    </div><br> 
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="input-group">
                                <span class="input-group-addon">Image</span>
                                <input type="file" class="form-control" placeholder="mentor Image" name="meta" id="edit_mentor_image">
                            </div>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="input-group">
                                <span class="input-group-addon">Contact No</span>
                                <input type="text" class="form-control" placeholder="Contact Number" name="contact_no" id="edit_mentor_contact_no">
                            </div>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="input-group">
                                <span class="input-group-addon">Contact Email</span>
                                <input type="email" class="form-control" placeholder="Contact Email" name="contact_email" id="edit_mentor_contact_email">
                            </div>
                        </div>
                    </div><br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
      </div>
</div><!-- /.modal -->
@endsection

@section('scripts')
<script type="text/javascript">
    $("#mentors_navbar").addClass("active");
    function edit_mentor(mentor_id){
        $("#preloader").show();
        var mentor_id, mentor_name, mentor_detail, mentor_description, mentor_pic;    
        $.ajax({
            url: '/mentors/get_mentor_detail/'+mentor_id,
            dataTyps: 'json',
            success: function(data){
                $("#preloader").hide();
                $("#edit_mentor_id").val(data.mentor_id);
                $("#edit_mentor_title").val(data.mentor_name);
                $("#edit_mentor_description").val(data.mentor_description);
                $("#edit_mentor_detail").val(data.mentor_detail);
                $("#edit_mentor_contact_no").val(data.mentor_contact_no);
                $("#edit_mentor_contact_email").val(data.mentor_contact_email);
                $("#edit_mentor_meta").val(data.mentor_pic);
                $("#edit_mentor_form").attr('action',"/mentors/"+data.mentor_id);
                $("#edit_mentor_modal").modal('show');
            },
            error: function(){
                alert("Seems like you are not connected to the internet");
                $("#preloader").hide();
            }
        });
    }
    function delete_mentor(mentor_id){
        alert(mentor_id);
    }
    function approve_mentor(mentor_id){
        $("#preloader").show();
        $.ajax({
            url: '/mentors/approve_mentor/'+mentor_id,
            dataType: 'json',
            success: function(data){
                if(data.flag == true){
                    $("#message_call").text(data.message);
                    $("#mentor_"+mentor_id).html("<span class=\"label label-success\">Approved</span>");
                }else{
                    $("#message_call").text(data.message);
                }
                $("#preloader").hide();
                $("#message_modal").modal('show');
            },
            error: function(data){
                $("#message_call").text(data.message);
                $("#preloader").hide();
                $("#message_modal").modal('show');
            }
        });
    }
    function unapprove_mentor(mentor_id){
        $("#preloader").show();
        $.ajax({
            url: '/mentors/unapprove_mentor/'+mentor_id,
            dataType: 'json',
            success: function(data){
                if(data.flag == true){
                    $("#message_call").text(data.message);
                    $("#mentor_"+mentor_id).html("<span class=\"label label-danger\">Unapproved</span>");
                }else{
                    $("#message_call").text(data.message);
                }
                $("#preloader").hide();
                $("#message_modal").modal('show');
            },
            error: function(data){
                $("#message_call").text(data.message);
                $("#preloader").hide();
                $("#message_modal").modal('show');
            }
        });
    }
</script>
@endsection