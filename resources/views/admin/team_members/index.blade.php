@extends('layouts.admin_panel')
@section('title', 'Admin Panel')

@section('content')
@parent
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Team Members
        <small>Entrepreneurship Summit 2k17</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">team_members</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Team Members Detail</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>#</th> 
                <th>Team Member Pic</th>
                <th>Team Member Name</th>
                <th>Status</th>
                <th>LinkedIn</th>
                <th>Posotion</th>
                <th>Modified At</th>
                <th>Edit</th>
                <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($team_members as $team_member)
                    <tr align="center">
                        <td>{{ $team_member->id }}</td>
                        <td>
                            @if($team_member->meta == '')
                                <img src="/images/esummit_black.png" alt="E-Summit" class="img-rounded" width="75px">
                            @else
                                <img src="/uploads/team_members/{{ $team_member->meta }}" alt="E-Summit" class="img-rounded" height="75px" >
                            @endif
                        </td>
                        <td>{{ $team_member->name }}</td>
                        <td>
                            <h4 id="team_member_{{ $team_member->id }}">
                            @if ($team_member->status === 'unapproved')
                                <span class="label label-danger">Unapproved</span>
                            @else
                                <span class="label label-success">Approved</span>
                            @endif
                            </h4>
                            <br>
                            <button type="button" class="btn btn-default" onclick="approve_team_member({{ $team_member->id }})">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                            </button>
                            <button type="button" class="btn btn-default" onclick="unapprove_team_member({{ $team_member->id }})">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                            </button>
                        </td>
                        <td>{{ $team_member->linkedin }}</td> 
                        <td>{{ $team_member->position }}</td>                                       
                        <td>{{ $team_member->updated_at }}</td>
                        <td><button type="button" class="btn btn-default" onclick="edit_team_member({{ $team_member->id }})"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button></td>
                        <td><button type="button" class="btn btn-default" onclick="delete_team_member({{ $team_member->id }})"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>#</th> 
                    <th>Team Member Pic</th>
                    <th>Team Member Name</th>
                    <th>Status</th>
                    <th>LinkedIn</th>
                    <th>Position</th>
                    <th>Modified At</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          

        </section>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
<div class="modal fade" tabindex="-1" role="dialog" id="edit_member_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form role="form" enctype="multipart/form-data" method="POST" action="/team_members/update_team_member" id="edit_team_member_form">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <input type="hidden" name="team_member_id">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel">Edit team_member</h4>
                </div>
                <div class="modal-body">
                <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="member_title">Name</span>
                            <input type="text" class="form-control" placeholder="Member Name" name="name" id="edit_member_name" required>
                        </div>
                    </div>
                </div><br>                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon">Position</span>
                            <select class="form-control" name="member_position" id="edit_member_position">
                              <option value ="overall">Overall Co-Ordinator</option>
                              <option value ="head">Head Co-Ordinator</option>
                              <option value ="managers">Manager</option>
                              <option value="executive">Executive</option>
                            </select>
                        </div>
                    </div>
                </div><br>                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="event_image">Image</span>
                            <input type="file" class="form-control" placeholder="Member Image" name="meta">
                        </div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="event_time">LinkedIn</span>
                            <input type="url" class="form-control" placeholder="LinkedIn" name="linkedIn" id="edit_member_linkedIn" required>
                        </div>
                    </div>
                </div><br>                
            </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
      </div>
</div><!-- /.modal -->
<script type="text/javascript">
    // $("#team_members_navbar").addClass("active");
    function edit_team_member(team_member_id){
        $("#preloader").show();
        var team_member_id, team_member_name, team_member_detail, team_member_description, team_member_pic;    
        $.ajax({
            url: '/team_members/get_team_member_detail/'+team_member_id,
            dataTyps: 'json',
            success: function(data){
                $("#preloader").hide();
                $("#edit_member_id").val(data.team_member_id);
                $("#edit_member_name").val(data.team_member_name);
                $("#edit_member_position").val(data.team_member_position);
                $("#edit_member_pic").val(data.team_member_pic);
                $("#edit_member_linkedIn").val(data.team_member_linkedIn);
                $("#edit_member_form").attr('action',"/team_members/"+data.team_member_id);
                $("#edit_member_modal").modal('show');
            },
            error: function(){
                alert("Seems like you are not connected to the internet");
                $("#preloader").hide();
            }
        });
    }
    function delete_team_member(team_member_id){
        alert(team_member_id);
    }
    function approve_team_member(team_member_id){
        console.log(team_member_id);
        $("#preloader").show();
        $.ajax({
            url: '/team_members/approve_team_member/'+team_member_id,
            dataType: 'json',
            success: function(data){
                if(data.flag == true){
                    $("#message_call").text(data.message);
                    $("#team_member_"+team_member_id).html("<span class=\"label label-success\">Approved</span>");
                }else{
                    $("#message_call").text(data.message);
                }
                $("#preloader").hide();
                $("#message_modal").modal('show');
            },
            error: function(data){
                $("#message_call").text(data.message);
                $("#preloader").hide();
                $("#message_modal").modal('show');
            }
        });
    }
    function unapprove_team_member(team_member_id){
        console.log(team_member_id);
        $("#preloader").show();
        $.ajax({
            url: '/team_members/unapprove_team_member/'+team_member_id,
            dataType: 'json',
            success: function(data){
                if(data.flag == true){
                    $("#message_call").text(data.message);
                    $("#team_member_"+team_member_id).html("<span class=\"label label-danger\">Unapproved</span>");
                }else{
                    $("#message_call").text(data.message);
                }
                $("#preloader").hide();
                $("#message_modal").modal('show');
            },
            error: function(data){
                $("#message_call").text(data.message);
                $("#preloader").hide();
                $("#message_modal").modal('show');
            }
        });
    }
</script>
@endsection