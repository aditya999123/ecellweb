@extends('layouts.admin_panel')
@section('title', 'Admin Panel')

@section('content')
@parent
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Leaderboard
        <small>B-Quiz 2k17</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Approve Answers</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Answers</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Username</th>
                  <th>Contact No</th>
                  <th>Marks</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($users as $user)
                    <tr align="center">
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $user['username'][0]->name }}</td>
                        <td>{{ $user['username'][0]->contact_no }}</td>
                        <td>{{ $user['marks'] }}</td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>#</th>
                    <th>Username</th>
                    <th>Contact No</th>
                    <th>Marks</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          

        </section>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $("#users_navbar").addClass("active");
    function approve_user(user_id){
        $("#preloader").show();
        $.ajax({
            url: '/admin/bquiz/approve_user/'+user_id,
            dataType: 'json',
            success: function(data){
                if(data.success == true){
                    $("#message_call").text(data.message);
                    $("#user_"+user_id).html("<span class=\"label label-success\">Approved</span>");
                }else{
                    $("#message_call").text(data.message);
                }
                $("#preloader").hide();
                $("#message_modal").modal('show');
            },
            error: function(data){
                $("#message_call").text("Sorry an error occured");
                $("#preloader").hide();
                $("#message_modal").modal('show');
            }
        });
    }
    function unapprove_user(user_id){
        $("#preloader").show();
        $.ajax({
            url: '/admin/bquiz/unapprove_user/'+user_id,
            dataType: 'json',
            success: function(data){
                if(data.success == true){
                    $("#message_call").text(data.message);
                    $("#user_"+user_id).html("<span class=\"label label-danger\">Unapproved</span>");
                }else{
                    $("#message_call").text(data.message);
                }
                $("#preloader").hide();
                $("#message_modal").modal('show');
            },
            error: function(data){
                $("#message_call").text(data.message);
                $("#preloader").hide();
                $("#message_modal").modal('show');
            }
        });
    }
</script>
@endsection