@extends('layouts.admin_panel')
@section('title', 'Admin Panel')

@section('content')
@parent
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Approve Answers
        <small>B-Quiz 2k17</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Approve Answers</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Answers</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Question ID</th>
                  <th>User ID</th>
                  <th>Answers</th>
                  <th>Status</th>
                  <th>Modified At</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($answers as $answer)
                    <tr align="center">
                        <td>{{ $answer->id }}</td>
                        <td>{{ $answer->question_id }}</td>
                        <td>{{ $answer->user_id }}</td>
                        <td>{{ $answer->answer }}</td>
                        <td>
                            <h4 id="answer_{{ $answer->id }}">
                            @if ($answer->status === 'unapproved')
                                <span class="label label-danger">Unapproved</span>
                            @else
                                <span class="label label-success">Approved</span>
                            @endif
                            </h4>
                            <br>
                            <button type="button" class="btn btn-default" onclick="approve_answer({{ $answer->id }})">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                            </button>
                            <button type="button" class="btn btn-default" onclick="unapprove_answer({{ $answer->id }})">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                            </button>
                        </td>
                        <td>{{ $answer->updated_at }}</td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                <th>#</th>
                <th>Question ID</th>
                <th>User ID</th>
                <th>Answers</th>
                <th>Status</th>
                <th>Modified At</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          

        </section>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $("#answers_navbar").addClass("active");
    function approve_answer(answer_id){
        $("#preloader").show();
        $.ajax({
            url: '/admin/bquiz/approve_answer/'+answer_id,
            dataType: 'json',
            success: function(data){
                if(data.success == true){
                    $("#message_call").text(data.message);
                    $("#answer_"+answer_id).html("<span class=\"label label-success\">Approved</span>");
                }else{
                    $("#message_call").text(data.message);
                }
                $("#preloader").hide();
                $("#message_modal").modal('show');
            },
            error: function(data){
                $("#message_call").text("Sorry an error occured");
                $("#preloader").hide();
                $("#message_modal").modal('show');
            }
        });
    }
    function unapprove_answer(answer_id){
        $("#preloader").show();
        $.ajax({
            url: '/admin/bquiz/unapprove_answer/'+answer_id,
            dataType: 'json',
            success: function(data){
                if(data.success == true){
                    $("#message_call").text(data.message);
                    $("#answer_"+answer_id).html("<span class=\"label label-danger\">Unapproved</span>");
                }else{
                    $("#message_call").text(data.message);
                }
                $("#preloader").hide();
                $("#message_modal").modal('show');
            },
            error: function(data){
                $("#message_call").text(data.message);
                $("#preloader").hide();
                $("#message_modal").modal('show');
            }
        });
    }
</script>
@endsection