@extends('layouts.app')
@section('title', 'Events')

@section('content')
@parent
<style>
    #myCarousel li img{
    height:120px;
    width:172px;
    padding: 0;
    margin: 0;
    padding-right: 5px;
    padding-left: 5px;
    float: left;
}
</style>
    <section class="slide-wrapper">
        <div class="container">
            <div id="myCarousel" class="carousel slide">
                @if($events->count() !== 0)
                <!-- Indicators -->
                <ol class="carousel-indicators" id="events_indicator">
                    
                    @foreach ( $events as $event)
                        @if ( $loop->index == 0)
                            <li data-target="#myCarousel" data-slide-to="{{ $loop->index }}" class="active"></li>
                        @else
                            <li data-target="#myCarousel" data-slide-to="{{ $loop->index }}"></li>
                        @endif
                    @endforeach
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" id="events_container">
                    @foreach ( $events as $event)     
                        @if($loop->index == 0)                   
                            <div class="item item{{ $loop->index + 1 }} active">
                        @else
                            <div class="item item{{ $loop->index + 1 }}">
                        @endif
                            <div class="fill" style=" background-color:#48c3af;">
                                <div class="inner-content">
                                    <div class="carousel-img">
                                        <img src="/uploads/events/{{ $event->meta }}" class="img img-responsive" />
                                    </div>
                                    <div class="carousel-desc">
                                        <h2>{{ $event->title }}</h2>
                                        <p>{{ $event->description }}</p>
                                        <!-- <button type="button" class="btn btn-primary" id="startup">Read more</button> -->
                                        <br><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                @else
                <div style="color: #fff;background-color:black;padding:200px 15px;text-align:left;">
                    <div class="text-center">
                        <h2>Events</h2>
                        <hr style="width:20%;">
                        <br>
                        <br>
                        <p>
                            <p>
                                <h2>Coming soon</h2>
                            </p>
                        </p>
                    </div>
                </div>
                @endif
            </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right"></span>
              <span class="sr-only">Next</span>
            </a>
        </div>
    </section>          
@endsection

@section("scripts")
<script>
    $("#events_navbar").css({'color':'white'});
    // var loadEvents = function() {
    //     $("#events_navbar").addClass("active");
    //     $.ajax({
    //             url: '/events/get_events_list',
    //             success: function(data){
    //                 var container = $('#events_container');
    //                 var container_indicator = $('#events_indicator');
    //                 var slide = '';
    //                 var indicator = '';
    //                 for(i in data){
    //                     if(i != 0){
    //                             //slide += "<div class=\"item item"+(parseInt(i)+1)+" active\">";
    //                             //indicator = "<li data-target=\"#myCarousel\" data-slide-to=\""+i+"\" class=\"active\"></li>";

    //                         slide += "<div class=\"item item"+(parseInt(i)+1)+"\">";
    //                         indicator += "<li data-target=\"#myCarousel\" data-slide-to=\""+i+"\"></li>";
    //                         slide += "<div class=\"fill\" style=\" background-color:#48c3af;\">";
    //                         slide += "<div class=\"inner-content\">";
    //                         slide += "<div class=\"carousel-img\">";
    //                         slide += "<img src=\"/uploads/events/"+ data[i]['meta'] +"\" class=\"img img-responsive\" />";
    //                         slide += "</div>";
    //                         slide += "<div class=\"carousel-desc\">";
    //                         slide += "<h2>"+data[i]['title']+"</h2><p>";
    //                         slide += data[i]['description'];
    //                         slide += "</p>";
    //                         slide += "<button type=\"button\" class=\"btn btn-primary\" id=\"startup\">Read more</button>";
    //                         slide += "</div></div></div></div>";
    //                         console.log(i);
    //                     }
    //                 }
    //                 console.log(slide);
    //                 container.append(slide);
    //                 container_indicator.append(indicator);
    //                 $('#myCarousel').carousel();
    //             },
    //             error:function(){
    //                 alert("It seems like you are not connected to the internet");
    //             }
            
    //         });
    //     }
            $('#myCarousel').carousel({
                interval:5000
            }); 
</script>
@endsection
