@extends('layouts.app')
@section('title', 'Vision')

@section('content')
    <div style="color: #777;background-color:white;padding:50px 15px;text-align:left;">
        <div class="text-center">
            <h2>About Us</h2>
            <hr style="width:20%;">
            <br>
            <center>
            <p style="color:black; max-width: 820px">Entrepreneurship Cell of NIT Raipur is non-profit organization working towards promoting the spirit of Entrepreneurship across Chhattisgarh. It aims to promote a startup-culture among denizens of NIT Raipur and equip them with the necessary tools to do so.
            <p style="color:black; max-width: 820px">A dedicated organisation working for more than half a decade, E Cell NIT Raipur has come a long way, establishing itself as one of the most effectively working Ecells with more than 60 motivated members.
            </center>
            <br>
        </div>
    </div>
    <div style="position:relative;">
        <div style="color:#ddd;background-color:#282E34;padding:50px 15px;text-align: left;">
            <div class="text-center">
                <h2>Vision</h2>
                <hr style="width:20%;">
                <br>
                <center>
                <p style="color: #ddd; max-width:820px">Our goal is to connect budding startups in Chhattisgarh with prominent venture capitalists, and angel investors and clientele in India. E-Cell seeks to achieve its goals by organizing various workshops, seminars and events. Over the years, E-Cell NIT Raipur has organised events like “Wall Street”, "Criconometrica",“E-fair",“HR Sutra” and several workshops in collaboration with Government of India (MSME) to help students cultivate required acumen to survive and thrive in today’s market. “E-Summit”, the flagship event of E-Cell has catered to the entrepreneurial appetite of its followers.</p>
                </center>
                <br>
            </div>

        </div>
    </div>
    <script>
        $("#vision_navbar").css({'color':'white'});
    </script>
@endsection