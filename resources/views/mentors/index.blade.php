@extends('layouts.app')
@section('title', 'mentors')

@section('content')
@parent
<link rel="stylesheet" href="/css/mentor.css">
<section id="mentor" style="padding:50px 15px;text-align:left;">
<div class="row">
    <h1 class="text-center">Our Mentors</h1>
    <hr style="width:30%;">
</div>
<div class="row" id="mentors_list">
</div>

</section>
@endsection

@section('scripts')
<script>
    $("#mentors_navbar").css({'color': 'white'});
    $.ajax({
        url: '{{ route("get_mentors_list") }}',
        success:function(data){
            var mentors = '';
            if(data.success == true){
                for(i in data.mentors){
                    mentors += "<div class=\"col-sm-6 col-md-4 col-lg-4 mt-4\">";
                    mentors += "<div class=\"card\"style=\"width:80%;\">";
                    mentors += "<div class=\"card-content\">";
                    mentors += "<img class=\"card-img-top\" src=\""+data.mentors[i]['image']+"\">";
                    mentors += "<div class=\"card-block\">";
                    mentors += "<h4 class=\"card-title text-primary text-center\">"+data.mentors[i]['name']+"</h4>";                    
                    mentors += "<div class=\"card-text\"><center>";
                    mentors += data.mentors[i]['description'];
                    mentors += "<p>"+data.mentors[i]['details']+"</p>";
                    mentors += "</center></div></div></div></div></div>";
                    if((parseInt(i)+1)%3 == 0 && i != 0){
                        mentors += "</div>";
                        mentors += "<div class=\"row\">";                                            
                    }    
                }
                console.log(mentors);
                $("#mentors_list").html(mentors);
            }
            else{
                mentors += "<center><h2>"+data.message+"</h2></center>";
                $("#mentors_list").html(mentors);
            }
        },
        error:function(){
            alert('Hey I am empty!!!! :(');
        }
    });
</script>
@endsection
