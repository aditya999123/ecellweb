<!doctype html>
<html lang="en">

<head>
  <title>Coming Soon | E-Cell NIT Raipur</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
    crossorigin="anonymous">
  <!-- custom css -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="/css/new_index.css">
  <meta name="title" content="E-Cell, NIT Raipur">
  <meta name="ROBOTS" content="Entrepreneurship Cell of NIT Raipur is non-profit organization working towards promoting the spirit of Entrepreneurship across Chhattisgarh. It aims to promote a startup-culture among denizens of NIT Raipur and equip them with the necessary tools to do so.">
  <meta name="description" content="Entrepreneurship Cell of NIT Raipur is non-profit organization working towards promoting the spirit of Entrepreneurship across Chhattisgarh. It aims to promote a startup-culture among denizens of NIT Raipur and equip them with the necessary tools to do so.">
  <meta name="abstract" content="LEADERS BEYOND BORDERS">

  <meta property="og:title" content="E-Cell | Home" />
  <meta property="og:type" content="Website" />
  <meta property="og:image" content="https://ecell.nitrr.ac.in/images/ecell.png" />
  <meta property="og:url" content="https://ecell.nitrr.ac.in/" />
  <meta property="og:description" content="Entrepreneurship Cell of NIT Raipur is non-profit organization working towards promoting the spirit of Entrepreneurship across Chhattisgarh. It aims to promote a startup-culture among denizens of NIT Raipur and equip them with the necessary tools to do so." />
  <meta property="fb:app_id" content="1687828501526245">
  
  <meta name="author" content="E-Cell NIT Raipur">
  <meta name="publisher" content="https://ecell.nitrr.ac.in">
  <meta name="copyright" content="https://ecell.nitrr.ac.in">
  <meta name="revisit-after" content="2 days">
</head>

<body>
  <div class="wrapper d-block d-sm-flex">
    <div class="left-space text-white">
      <!-- <img src="./logo-final.png" class="p-3" alt=""> -->
    </div>
    <div class="message w-100 d-sm-flex flex-column justify-content-center">
      <h1>
        WE ARE
        <br> COMING
        <br> SOON...
      </h1>
      <h3 class="mt-2">E-Cell NIT Raipur</h3>
      <p>
        Calling out the entrepreneur in you
        <br>A great start for better future
        <br>Stay tuned on social media
      </p>
    </div>
    <div class="social-links">
      <a href="https://www.facebook.com/ecellnitrr/" target="_blank">
        <i class="fa fa-2x fa-facebook"></i>
      </a>
      <a href="https://twitter.com/ecellnitrr?lang=en" target="_blank">
        <i class="fa fa-2x fa-twitter"></i>
      </a>
      <a href="https://www.instagram.com/ecell.nitraipur/" target="_blank">
        <i class="fa fa-2x fa-instagram"></i>
      </a>
      <a href="https://www.linkedin.com/company/entrepreneurship-cell-nit-raipur/" target="_blank">
        <i class="fa fa-2x fa-linkedin"></i>
      </a>
    </div>
  </div>
</body>

</html>