@extends('layouts.app')
@section('title', 'Home')
@section('image','https://ecell.nitrr.ac.in/images/ecell.png')

@section('content')
<style>
    .JesterBox div {
  visibility: hidden;
  position: fixed;
  top: 5%;
  right: 5%;
  bottom: 5%;
  left: 5%;
  z-index: 75;
  text-align: center;
  display: flex;
  justify-content: center;
  align-items: center;
}

.JesterBox div:before {
  content: '';
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 74;
  background-color: rgba(0, 0, 0, 0);
  transition: all 0.5s ease-out;
}

.JesterBox div img {
  position: relative;
  z-index: 77;
  max-width: 100%;
  max-height: 100%;
  margin-left: -9999px;
  opacity: 0;
  transition-property: all, opacity;
  transition-duration: 0.5s, 0.2s;
  transition-timing-function: ease-in-out, ease-out;
}

.JesterBox div:target { visibility: visible; }

.JesterBox div:target:before { background-color: rgba(0, 0, 0, 0.7); }

.JesterBox div:target img {
  margin-left: 0px;
  opacity: 1;
}
</style>
<a href="#" class="JesterBox">
    <div id="show_schedule"><img src="/storage/{{ setting('schedule') }}"></div>
</a>
    <div class="preloader">
        <img src="/images/ecell.png" alt="E-Cell NIT Raipur" height="200px" width="200px">
    </div> 
    <header id="intro">

        <div class="container">
            <div class="table">
                <div class="header-text">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <p>
                                <h1 class="light white">E-Summit'17</h1>
                                <h2 class="white typed"></h2>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section>
        <div class="cut cut-top"></div>
        <div class="container">
                <div class="row intro-tables">
                <!-- <div class="col-md-4">
                    <div class="intro-table intro-table-first">
                        <h4 class="white heading">Schedule</h4>
                        <div class="owl-carousel owl-schedule bottom">
                            <div class="item" id="schedule">
                            </div>                   
                        </div>
                    </div>
                </div> -->
                <div class="col-md-6">
                    <div class="intro-table intro-table-first">
                        <h4 class="white heading">UPDATES</h4>
<!--                         <div class="owl-carousel bottom">
                            <h6 class="white heading small-heading no-margin regular">Portal to be opened soon</h6>
                            <a class="btn btn-white-fill expand">Portal yet to be opened</a>
                        </div> -->
                        <div class="registerlink">
                            <a href="https://goo.gl/forms/2MzkD7ANZ8HcTgW52" style="color:yellow">Register Now for ESummit </a>
                        </div>
                        <div class="registerlink">
                            <a href="/bmodel" style="color:yellow">B-Model</a>
                        </div>
                        <div class="registerlink">
                            <a href="https://play.google.com/store/apps/details?id=com.bquiz.raipur.ecellapp2k17" style="color:yellow">Android App Launched</a>
                        </div>
                        <div class="registerlink">
                            <a href="/pdf/rules_bmodel.pdf" style="color:yellow" download>Download rules/guidelines for BModel from here</a>
                        </div>
                        <div class="registerlink">
                            <a href="#show_schedule" style="color:yellow">Schedule Launched</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="intro-table intro-table-third">
                        <h4 class="white heading">NOTIFICATIONS</h4>
                        <div class="item">
                            <div class="schedule-row row">
                                <div class="col-xs-12">
                                    <ul>
                                        <li>
                                            <div class="registerlink">
                                                <a href="#" style="color:yellow">IGNITION the B-Model Competition Update</a>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="registerlink">
                                                <a href="/bmodel" style="color:yellow">The deadline for abstract submission is extended to 7th September Midnight</a>
                                            </div>
                                        </li>
                                        <li>
                                        <div class="registerlink">
                                            <a href="#show_schedule" style="color:yellow">Schedule Launched</a>
                                        </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="events" class="section">
        <div class="container">
            <div class="row text-center title">
                <h2>Some highlights</h2>
                <p class="light muted">The Entrepreneurship Summit is not just an event to provide platform where the students, faculty, investors, participants, speakers and start-ups can interact with each other and gain traction to improve them. We believe in a growth where everyone grows together and hence is this summit. The bright minds shall gain the motivation, resources and the pathway to realize their potential and convert their ideas into innovative start-ups.</p>
            </div>
            <div class="row highlights">
                <div class="col-md-4">
                    <div class="highlight">
                        <div class="icon-holder">
                            <img src="/images/Workshop-1.png" alt="" class="icon">
                        </div>
                        <h4 class="heading">Workshop on Data Analysis and impacts on the modern world</h4>
                        <p class="description">This workshop shall be conducted to introduce the field of data analysis and data security to the stakeholders.The students shall be given hands on experience with real world data to work on and see how they affect the systems around us.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="highlight">
                        <div class="icon-holder">
                            <img src="/images/Workshop-2.png" alt="" class="icon">
                        </div>
                        <h4 class="heading">Workshop on Government policies for Indian start-ups:</h4>
                        <p class="description">The key policy changes introduced by the Government in Start-up India action plan are discussed in this workshop organized by Entrepreneurship Cell, NIT Raipur.
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="highlight">
                        <div class="icon-holder">
                            <img src="/images/Speaker-Session.png" alt="" class="icon">
                        </div>
                        <h4 class="heading">Speaker Sessions</h4>
                        <p class="description">The sessions shall consist of 6 speakers from the industry who have been active in the start-up world and have established successful ventures. The speakers shall have their sessions on different topics that are common to the industry like start-up funding, early stage investing, operations, management and other related topics. </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="cut cut-bottom"></div>
    </section>
    <section id="summmary">
        <div style="color:#fff;background-color:#282E34;padding:15px 15px;text-align: left;">
            <h2 class="heading text-center">E-Summit '17</h2>
            <div class="fullarticle" id="article1" style="display:block;">
                <h3 class="heading text-center">Summary</h3>
                <p>Entrepreneurship cell wants to organize an Entrepreneurship Summit to promote entrepreneurship among students and also provide a start-up initiative activity with help of NIT Raipur. It will be a conclave of some of the greatest minds in India ranging from students to investors and established startup founders. This is to inspire the student community to come up with innovative ideas and implement them for the betterment of the society. The event consists of speaker sessions, workshops and startup camp all of which are designed to enthuse in a spirit of innovation, and entrepreneurship among the students and show the real implementation. E-Cell will require the help of NIT Raipur administration to effectively organize the event.
            </div>
            <div class="fullarticle" id="article2">
                <h3 class="heading text-center">Into E-Summit '17</h3>
                <p>Entrepreneurship Cell is a committee in NIT Raipur working towards promoting Entrepreneurship in Chhattisgarh. It is also working towards to promote a start-up culture among denizens of NIT Raipur and to provide them all the facilities they will need to start-up.
                    <p>Our aim is to connect budding start-ups in Chhattisgarh with investors and clients in India. E-Cell achieves its goals by organizing various workshops, seminars and events. Our Flagship Event “E-Summit” and “B-Plan” have benefitted a lot of people and has provided them with the motivation and push needed to start up. E-Cell aims to adhere to the vision of our honorary Prime Minister “Mr. Narendra Modi” who has started a initiative called “Start-up India Stand Up India”
                        <p>We aim to target potential entrepreneurs in Chhattisgarh. These mainly include the students of NIT Raipur, IIIT Raipur, IIM Raipur, HNLU Raipur, SSIPMT, BIT Durg and other colleges in India.
                            <p>The Entrepreneurship Summit is not just an event to provide platform where the students, faculty, investors, participants, speakers and start-ups can interact with each other and gain traction to improve them. We believe in a growth where everyone grows together and hence is this summit. The bright minds shall gain the motivation, resources and the pathway to realize their potential and convert their ideas into innovative start-ups.
            </div>
            <div class="fullarticle" id="article3">
                <h3 class="heading text-center">Need</h3>
                <p>The Entrepreneurship Summit is needed because of the following reasons:
                    <ol>
                        <li>Motivates young minds to innovate and use their potential.</li>
                        <li>Introduces and provides students the interaction with industry leaders and experts.</li>
                        <li>Provides knowledge about existing schemes helpful to start-ups</li>
                        <li>Introduces the concept of investment and funding to relevant audience</li>
                        <li>Introduces new fields of technology and provides hands on experience in the fields to innovate</li>
                        <li>Provides interaction with implemented start-ups and networking among them and the students as well </li>
                    </ol>
                    <p>Well established E-Cells such as those of IIT Kharagpur and IIT Bombay yearly organize an Entrepreneurship Summit and it has proved effective in yielding out entrepreneurs from the respective regions of these colleges. E-Cell NIT Raipur looks forward to replicate the same.
                        <p>Target Population of competition is the budding engineers. Most of the students are new to the concept of entrepreneurship and hence they have no idea as how to convert their path breaking ideas into production and thereby into a successful business venture. The primary focus of the Entrepreneurship Summit is to address this problem. It focuses first on motivating the students and introducing them the concept of entrepreneurship and then enables them to have interaction with established entrepreneurs and innovators across the country to learn from them and grow.
            </div>
            <div class="fullarticle" id="article4">
                <h3 class="heading text-center">Goals/Objectives </h3>
                <ul>
                    <li>Provide motivation to students for innovation</li>
                    <li>Promote networking among start-ups</li>
                    <li>Provide knowledge about existing and upcoming schemes for start-ups</li>
                    <li>Provide knowledge of budding and upcoming fields to innovate on</li>
                    <li>Promote Entrepreneurship</li>
                    <li>Provide knowledge about nuances of Managing and Operating a startup</li>
                    <li>Develop a start-up culture</li>
                    <li>Connecting students with different start-ups</li>
                    <li>Promote the start-ups</li>
                    <li>Improve NIT Raipur’s ranking by promoting startup culture in campus </li>
                </ul>
            </div>
            <div class="dotstyle dotstyle-tooltip text-center">
                <ul>
                    <li class="current"><a href="#">Summary</a></li>
                    <li><a href="#">Into E-Summit '17</a></li>
                    <li><a href="#">Need</a></li>
                    <li><a href="#">Goals/Objectives </a></li>
                </ul>
            </div>
        </div>
    </section>
    <section id="thinkraipur">
	    <div style="color:black;background-color:#eee;padding:50px 15px;">
            <div class="text-center">
				<img src="https://www.thinkraipur.in/img/logo.png?v=1" width="300px"><br><br>
            </div>
            <h2 class="text-center">
                <b style="color: black">DO YOU HAVE AN </b><b style="color: #ed5b24">IDEA</b><b style="color: black"> THAT CAN </b><b style="color: #ed5b24"> TRANSFORM</b><b style="color: black"> YOUR </b><b style="color: #ed5b24">CITY</b><b style="color: black">? </b>
            </h2>
            <p style="color: black;"></style><span style="color: #ed5b24"><a href="https://www.thinkraipur.in/index.php" target="_blank">Think Raipur</a></span> is a wide scaled organised event aimed at getting ideas from people across the country to create india's first people-powered smart city.Participants will have the opportunity to transform their innovation - something as simple as a summer project or a research endeavor - into real world entrepreneurial ideas.<br>
            <div class="text-center"><a href="https://www.thinkraipur.in/index.php" target="_blank"><button type="button" class="btn btn-success">Learn more about the event</button></a></div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <h2 class="text-center heading">Supported By</h2>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-6" style="padding-top:3%;">
                        <img src="/images/Startindia.png" alt="" style="width: 70%;" class="center-block">
                </div>
                <div class="col-sm-12 col-md-6">
                        <img src="/images/digindia.png" alt="" style="width: 55%;"  class="center-block">
                </div>
            </div>
        </div>
                <!-- <p style="color:#fff;">The Entrepreneurship- Cell has collaborated with StartupIndia for this session. Startup India campaign is based on an action plan aimed at promoting bank financing for start-up ventures to boost entrepreneurship and encourage start ups with jobs creation. The campaign was first announced by Prime Minister Narendra Modi in his 15 August 2015 address from the Red Fort. The Reserve Bank of India said it will take steps to help improve the ‘ease of doing business’ in the country and contribute to an ecosystem that is conducive for the growth of start-up businesses.</p> -->

   
    
           <!-- <p style="color: #080808; "><b>The Digital India programme is a flagship programme of the Government of India with a vision to transform India into a digitally empowered society and knowledge economy.</b>

            <p style="color: #080808; ">The journey of e-Governance initiatives in India took a broader dimension in mid 90s for wider sectoral applications with emphasis on citizen-centric services. Later on, many States/UTs started various e-Governance projects. Though these e-Governance projects were citizen-centric, they could make lesser than the desired impact. Government of India launched National e-Governance Plan (NeGP) in 2006. 31 Mission Mode Projects covering various domains were initiated. Despite the successful implementation of many e-Governance projects across the country, e-Governance as a whole has not been able to make the desired impact and fulfil all its objectives.

            <p style="color: #080808; ">It has been felt that a lot more thrust is required to ensure e-Governance in the country promote inclusive growth that covers electronic services, products, devices and job opportunities. Moreover, electronic manufacturing in the country needs to be strengthened.

            <p style="color: #080808; ">In order to transform the entire ecosystem of public services through the use of information technology, the Government of India has launched the Digital India programme with the vision to transform India into a digitally empowered society and knowledge economy.-->
    </section>
    <section id="sponsor">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="text-center">
                        <h2 class="heading">Sponsors</h2>
                        <hr style="width:40%;">
                        <h3 class="heading">To be revealed soon!</h3>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </section>
     
    <section id="map">
        <div class="map-responsive">
            <iframe width="100%" height="450" scrolling="no" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJI2f2E-LdKDoRx0MMxWU5VCE&key=AIzaSyCq4vWNv6eCGe2uvhPRGWQlv80IQp8dwTE" allowfullscreen></iframe>
        </div>
    </section>        
    <section id="contact">
        <div class="row text-center title">
            <h2><br>Let's talk start-up</h2>
            <hr style="width:20%;">
            <br>
            <br>
        </div>
        <div class="row">
            <div class="col-lg-6 col-sm-7">
                <div class="contact-info-box address clearfix">
                    <h3><i class=" icon-map-marker"></i>Address:</h3>
                    <span>NIT Raipur,G.E. Road<br>Raipur, C.G. 492010.</span>
                </div>
                    <div class="contact-info-box phone clearfix">
                    <h3><i class="fa-phone"></i>Phone:</h3>
                    <span>8827544244</span>
                </div> 
                <div class="contact-info-box email clearfix">
                    <h3><i class="fa-pencil"></i>email:</h3>
                    <span>ecell@nitrr.ac.in</span>
                </div>
                <div class="contact-info-box hours clearfix">
                    <h3><i class="fa-clock-o"></i>Hours:</h3>
                    <span><strong>Monday - Friday: </strong>10AM-5PM</span>
                </div>
                <ul class="social-link">
                    <li class="twitter"><a href="https://twitter.com/ecellnitrr?lang=en" target="_blank"><i class="fa-twitter"></i></a></li>
                    <li class="facebook"><a href="https://www.facebook.com/ecellnitrr/" target="_blank"><i class="fa-facebook"></i></a></li>
                    <li class="linkedin"><a href="https://in.linkedin.com/in/e-cell-nit-raipur-a1682b122" target="_blank"><i class="fa-linkedin"></i></a></li>
                </ul>
            </div>
            <div class="col-lg-6 col-sm-5 wow fadeInUp delay-05s">
                <div class="form">

                    <div id="sendmessage">Your message has been sent. Thank you!</div>
                    <div id="errormessage"></div>
                    <!-- <form action="" method="post" role="form" class="contactForm"> -->
                        <div class="form-group">
                            <input type="text" name="contact_us_name" class="form-control input-text" id="contact_us_name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                            <div class="validation"></div>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control input-text" name="contact_us_email" id="contact_us_email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                            <div class="validation"></div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control input-text" name="contact_us_subject" id="contact_us_subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                            <div class="validation"></div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control input-text text-area" name="contact_us_message" id="contact_us_message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                            <div class="validation"></div>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="input-btn" id="send_button">Send Message</button>
                        </div>
                        <br>
                    <!-- </form> -->
                </div>
            </div>
        </div>
    </section>
@endsection
@section("scripts")
<script>
    $("#home_navbar").css({'color':'white'});
            [].slice.call(document.querySelectorAll('.dotstyle > ul')).forEach(function(nav) {
                new DotNav(nav, {
                    callback: function(idx) {
                        //console.log( idx )
                    }
                });
            });
    $.ajax({
        url: '/get_events_list',
        success: function(data){
            var list = '';
            if(data.success == true){
                var counter = 0;
                for(i in data.events){
                    counter = counter + 1;
                    list += "<div class=\"schedule-row row\">";
                    list += "<div class=\"col-xs-6\">";
                    list += "<p class=\"regular white\">"+data.events[i]['title']+"</p>";
                    list += "</div>";
                    list += "<div class=\"col-xs-6 text-right\">";
                    list += "<p class=\"white\">"+data.events[i]['date']+"</p>";
                    list += "</div>";
                    list += "</div>";
                    if(counter % 3 == 0){
                        list += "</div><div class=\"item\">";
                    }
                }
            }else{
                list += "<div class=\"schedule-row row\">";
                list += "<div class=\"col-xs-6\">";
                list += "<p class=\"regular white\">"+"Coming Soon"+"</p>";
                list += "</div>";
                list += "<div class=\"col-xs-6 text-right\">";
                list += "<p class=\"white\">"+"Coming Soon"+"</p>";
                list += "</div>";
                list += "</div>";
            }
            console.log(list);
            $("#schedule").html(list);
        },
        error: function(){
            alert('Sorry It seems like you are not conected to the internet');
        }
    });
    $("#send_button").click(function(){
        var name = $('#contact_us_name').val();
        var email = $('#contact_us_email').val();
        var subject =  $('#contact_us_subject').val();
        var message = $('#contact_us_message').val();
        $.ajax({
            url: "/submit_contact_us",
            type: "POST",
            data: {
                'name': name,
                'email': email,
                'message': subject,
                'subject': message
            },
            success: function(data){
                $("#errormessage").hide();
                $("sendmessage").hide();
                if(data.success == true){                    
                    $("#sendmessage").show();
                }else{
                    $("#errormessage").html(data.message);
                    $("#errormessage").show();
                }
            },
            error:function(){
                $("#errormessage").hide();
                $("sendmessage").hide();
                $("#errormessage").html("Sorry an error occured please try again after some time.");
                $("#errormessage").show();
            }
        });
    });
</script>
@endsection