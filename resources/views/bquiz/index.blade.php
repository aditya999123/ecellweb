@extends('layouts.bquiz')
@section('title', 'B-Quiz')

@section('style')
<style>
    .color-palette {
      height: 35px;
      line-height: 35px;
      text-align: center;
    }

    .color-palette-set {
      margin-bottom: 15px;
    }

    #time_indicator{
      padding-left: 10px;
      padding-right: 10px;
    }

    .color-palette span {
      /* display: none; */
      font-size: 20px;
    }

    .color-palette:hover span {
      display: block;
    }

    .color-palette-box h4 {
      position: absolute;
      top: 100%;
      left: 25px;
      margin-top: -40px;
      color: rgba(255, 255, 255, 0.8);
      font-size: 12px;
      display: block;
      z-index: 7;
    }

    #question_displayer {
      overflow-x:auto;
    }

    #timer-source {
      position: fixed;
      display: block;
      right: 0;
      bottom: 0;
      margin-right: 40px;
      margin-bottom: 40px;
      z-index: 900;
    }
  </style>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <div id="false-content">
    <div class="content-wrapper" style="min-height: 0px;">
        <section class="content">
          <div class="text-center">
            <img src="/images/B-Quiz.png" width="350" height="350">
            <br>
            <h1 class="text-center" id="data_message"><b></b></h1>
          </div>
        </section>  
      </div>
  </div>
  <div class="color-palette-set" id="timer-source">
    <div id="time_indicator"><span id="timer_p"></span></div>
  </div>
  <div class="container" id="lock">
    <div class="lockscreen-wrapper">
      <div class="lockscreen-logo">
        <a href="../../index2.html"><b>Entrepreneurship</b>Cell</a>
      </div>
      <!-- User name -->
      <center><div class="lockscreen-name">{{ Auth::user()->name }}</div></center<

      <!-- START LOCK SCREEN ITEM -->
      <div class="lockscreen-item">
        <!-- lockscreen image -->
        <div class="lockscreen-image">
          <img src="/storage/{{ Auth::user()->avatar }}" alt="User Image">
        </div>
        <!-- /.lockscreen-image -->

        <!-- lockscreen credentials (contains the form) -->
        <form class="lockscreen-credentials">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Please wait..." readonly id="lock_screen_message">
          </div>
        </form>
        <!-- /.lockscreen credentials -->

      </div>
      <!-- /.lockscreen-item -->
      <div class="help-block text-center" id="lockscreen_message">
      </div>
      <div class="text-center">
        <a href="#">Entrepreneurship Summit 2k17</a>
      </div>
      <div class="lockscreen-footer text-center">
        Trek to the zenith of glory
      </div>
    </div>
  </div>
  <div class="container" id="question_div">   
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Points
        <small id="points"></small>
      </h1>
    </section>   

    <!-- Main content -->
    <section class="content">        
      <!-- row -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-default">              
            <!-- /.box-header -->
            <!-- <div class="box-body">
              <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-info"></i> Messages!</h4>
                <p id="active_messages"></p>
              </div>
            </div> -->
            <!-- /.box-body -->
            <div class="box-body">
              <div class="row"> 
                <div class="col-sm-4 col-md-4 col-md-offset-4 col-sm-offset-4">
                  
                </div>
                <!-- /.col -->                                  
              </div>
              <!-- /.row -->                
            </div>
            <br>
            
              <div class="box box-widget" >
                  <div class="box box-widget widget-user ">                        
                    <center>
                      <img src=""  id="image_url" width="200" height="200" style="padding-bottom: 3%;">
                      <p id="question_displayer" style="color: #000; padding: 3%;"></p>
                      <div style="width: 70%; padding-left: 30%;">
                      </div>
                    </center>
                    <div class="text-center" style="width: 70%; padding-left: 30%;" id="answer_box">
                      <div>
                        <label for="answer"><h3><b>Answer</b></h3></label>
                        <input type="text" name="" class="form-control" id="answer" placeholder="Please enter your answer">
                        <button onclick="submit_answer()" type="button" class="btn bg-olive btn-flat margin btn-lg" ><b>Submit</b></button>
                      </div>
                    </div>
                    <br>                      
                  </div>
                  <!-- <textarea id="question" style="color: black; padding: 3%; width: 80%;"></textarea> -->
              </div>
            
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
</div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<script>
  var i, yellow, red;
  $(document).ajaxStart(function () {
    Pace.restart()
  })
  $("#lock").show();
  $("#question_div").hide();
  var is_bquiz_live = setInterval(function(){ is_bquiz_active() },3000);
  var question_id;
  $(document).ready(function(){
      $("#lock").hide();
      $("#question_div").hide();
      $("#true-content").hide();
      $("#false-content").hide();

      var timer = '';      
      var indicator = '';
  });

  function setTimer(){
    indicator = "Time left: "+i+" sec";
    $("#timer_p").html(indicator);
    if(i > yellow){
      $("#time_indicator").addClass("bg-green-active color-palette");
    }else if(i > red){
      $("#time_indicator").removeClass("bg-green-active color-palette");
      $("#time_indicator").addClass("bg-yellow-active color-palette");
    }else{
      $("#time_indicator").removeClass("bg-yellow-active color-palette");
      $("#time_indicator").addClass("bg-red-active color-palette");
    }
    i = i - 1;
    if(i == 0 || i < 0){
      clearInterval(timer);
      indicator = "Time Out";
      $("#timer_p").html(indicator);
      $("#time_indicator").addClass("bg-aqua-active color-palette");
      $("#answer_box").hide();
      $("#question_div").hide();
      $("#lock").show();
      // alert("Time Out");
      is_bquiz_live = setInterval(function(){ is_bquiz_active() },3000);
    }
  }
    
  function is_bquiz_active(){
    question_id = 0;
    $.ajax({
      url: '/is_bquiz_active',
      success: function(data){
        if(data.success == true){
          $("#true-content").show();
            $.ajax({
              url : "/get_live_question_web" ,
              success : function(data1){
                if(data1.success == true){
                  $("#lock").hide();
                  $("#answer_box").show();
                  $("#false-content").hide();
                  $("#question_div").show();
                  i = data1.question_data.question_duration;
                  yellow = i/2;
                  red = yellow/2;
                  timer = setInterval(function(){ setTimer() }, 1000);
                  $("#time_indicator").show();
                  $("#question_id").html(data1.question_data.question_id);
                  $("#points").html(data1.question_data.points);
                  $("#image_url").attr("src",data1.question_data.image_url);
                  $("#question_displayer").html(data1.question_data.question);
                  question_id = data1.question_data.question_id;
                  $("#question_duration").html(data1.question_data.question_duration);
                  console.log(data1.question_data);
                  clearInterval(is_bquiz_live);
                  if(data1.data_type == 2){                                
                    $("#image_url").attr("src",data1.question_data.image_url);
                  }
                }else{
                  $("#lock").show();
                  $("#question_div").hide();
                }
                  
              },

              error : function(){
                alert("Sorry an error occured");
              },
            });
        }else{
          $("#lock_screen_message").val(data.message);
          $("#question_div").hide();
          $("#false-content").show();
        }
      }
    });
  }

  function submit_answer(){
    $.ajax({
      url: '/submit_answer_web',
      type: 'POST',
      data: {
        'question_id' : question_id,
        'answer' : $("#answer").val(),
        '_token' : '{{ csrf_token() }}'
      },
      success: function(data){
        if(data.success == true){
          $("#true-content").show();
          $("#question_div").hide();
            $.ajax({
              url : "/get_live_question_web" ,
              success : function(data1){
                if(data.success == true){
                  $("question_div").hide();
                  $("#lock").show();
                  $("#false-content").hide();
                  $("#lock_screen_message").val(data.message);
                  $("#time_indicator").hide();
                  is_bquiz_live = setInterval(function(){ is_bquiz_active() },3000);
                } else{
                  $("#lock").show();
                  $("question_div").hide();
                  $("#lock_screen_message").val(data.message);
                } 
              },

              error : function(){
                alert("Sorry an error occured");
              },
            });
        }else{
          $("#lock_screen_message").val(data.message);
          $("data_message").val(data.message);
          $("#question_div").hide();
          $("#lock").show();
        }
      },
      error: function(data){
        $("#lock").show();
        $("#question_div").hide();
        $("#lock_screen_message").val('Sorry an error occured');
      }
    });
  }
</script>
@endsection