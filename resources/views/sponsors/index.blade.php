@extends('layouts.app')
@section('title', 'Sponsors')

@section('content')
@parent
<!-- Sponsor -->
<link href="/css/sponsor.css" rel="stylesheet">
<section id="sponsor" style="padding:50px 15px;text-align:left;">
    <div class="text-center">
        <h2>Our sponsors</h2>
        <hr style="width:20%;">
        <br>
    </div>
    <ul class="ch-grid" id="sponsors_list">
    </ul>
</section>
@endsection

@section('scripts')
<script>
    $("#sponsors_navbar").css({'color': 'white'});
    $.ajax({
        url: '{{ route("get_sponsors_list") }}',
        success:function(data){
            var sponsors = '';
            var counter = 0;
            if(data.success == true){
                for(i in data.spons){
                    for(j in data.spons[i]['sponsors']){
                        counter = counter + 1;
                        sponsors += "<li>";
                        sponsors += "<div class=\"ch-item\">";
                        sponsors += "<div class=\"ch-info\">";
                        sponsors += "<div class=\"ch-info-front\" style=\"background-image: url("+data.spons[i]['sponsors'][j]['image1']+"); background-repeat:no-repeat; background-size: 100%; background-position:center;\"></div>";
                        sponsors += "<div class=\"ch-info-back\"><center>";
                        sponsors += "<h3>"+data.spons[i]['sponsors'][j]['sponsName']+"</h3>";
                        sponsors += "<p>"+data.spons[i]['sponsors'][j]['body']+"<br>";
                        sponsors += "<a href =\""+data.spons[i]['sponsors'][j]['website_url']+"\">"+data.spons[i]['sponsors'][j]['website_url']+"</a>";
                        sponsors += "</p></center></div></div></div></li>";
                        if(counter%3 == 0 && i != 0){
                            sponsors += "</ul>";
                            sponsors += "<ul class=\"ch-grid\">";
                        }
                    }    
                }
                // if(i%3 == 0 || i == 0){
                //     sponsor += "</ul>";
                // }
                console.log(sponsors);
                $("#sponsors_list").html(sponsors);
            }
            else{
                sponsors += "<center><h2>Coming Soon</h2></center>";
                $("#sponsors_list").html(sponsors);
            }
        },
        error:function(){
            alert('Hey I am empty!!!! :(');
        }
    });
</script>
@endsection
