@extends('layouts.app')
@section('title', '{{ $blog->title }}')

@section('content')
<div class="wrapper">
    <div class="container">
    <br>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 mt-12">
                <div class="card">
                    <div class="card-content">
                        <center>
                            <img class="card-img-top" src="/storage/{{ $blog->image}}">
                            <div class="card-block">
                                <h4 class="card-title text-primary">{{ $blog->title }}</h4>
                                <!-- <div class="meta">
                                    <p class="text-info">In 1999 Time Magazine named Bezos its 1999 Person of the Year.</p>
                                </div> -->
                                <div class="card-text">
                                    {!! $blog->body !!}
                                </div>
                            </div>
                            <div class="card-footer">
                                <span class="glyphicon glyphicon-time"></span> Posted by Aman Bhalla
                                <span><i class=""></i>{{ $blog->updated_at }}</span>
                            </div>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("#blogs_navbar").css({'color':'white'});
</script>
@endsection