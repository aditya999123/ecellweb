@extends('layouts.app')
@section('title', 'B-Model')
@section('page_link','bmodel')
@section('image','https://www.happychases.com/wp-content/uploads/2017/08/20988141_1517227561669709_1434422436402186662_o.jpg')

@section('content')
<style>
    @media only screen and (max-width: 480px){
    .mobshow  {display: inline-block !important; width:auto !important; height:auto !important; overflow:visible !important; visibility:visible !important;}

    .mobnotshow  {display: none !important; width:auto !important; height:auto !important; overflow:visible !important; visibility:visible !important;}
    }

    iframe {

        width: 100%;
        /*overflow:hidden;*/
        /*min-height:100%;*/
        display:inline;
        padding-left:5%;
        padding-right:5%;
        /* height:400%; */
        /* position:absolute; */
    }
    /* section{
        height: 100%;
    } */
    /* ::-webkit-scrollbar {
        display: none;
    } */
    body{
        background-color:#533E2D;
	    color:#fff;
    }
</style>
<div class="row">
    <div class="col-lg-12" style="padding-left: 5%; padding-right: 5%">
	<br>
	<br>
	<br>
        <h1 class="text-center">B-Model</h1>
        <hr style="width:30%;">
        <p>
            Ignition is the business model competition designed to emulate the process of the growth of an idea
            towards a full-fledged startup. The basic motive of this B-model is to connect the budding
            entrepreneurs with investors giving start-ups a leap. Right from acknowledging that your idea has
            potential, to drafting a B-Model, and pitching in front of an esteemed panel of investor- this
            platform has it all. The stages of the B-model competition include-
        </p>
        <ul>
            <li>Workshop – 27 th August</li>
            <li>Submission of model</li>
            <li>Presentation</li>
            <li>Mentorship</li>
            <li>Final round</li>
        </ul>
        <p>
            B-model is indeed of great importance because along with giving clarity for business, it provides the
            budding entrepreneurs the required details about a business including the target audience,
            competitors and scope in the future. Also the B-model contains the relevant information that is
            required by the investor. The primary motive for implementing this competition in our college is
            solely intended to set alight the entrepreneurial spirits among the students so as to educate and
            inspire smarter entrepreneurs who want to launch ventures that are more successful.
        </p>
    </div>
</div>
<br>
<div class="row mobshow">
    <div class="col-lg-12" style="padding-left: 5%; padding-right: 5%">
        <a href="/pdf/rules_bmodel.pdf" download>Download rules/guidelines from here</a>
    </div>

</div>
<div class="row mobnotshow">
    <!-- <div class="col-lg-12" style="padding-right:100px;padding-left: 100px;height:400%"> -->
        <!-- <iframe height="1000px" src="https://ecell.nitrr.ac.in/pdf/rules_bmodel.pdf">Loading...</iframe> -->
        <embed src="/pdf/rules_bmodel.pdf" width="100%" height="500" type='application/pdf' id="embeded_pdf">
    <!-- </div> -->
</div>

<!-- 
<section id="mentor">

</section> -->
@endsection

@section('scripts')
<!--<script>
    var isMobile = false;
    if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true
    console.log(isMobile);
</script> -->
@endsection
