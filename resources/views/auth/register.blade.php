<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>E-Cell | Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="/plugins/iCheck/square/blue.css">

  <link rel="icon" href="/images/ecell.png">

  <!-- Pace style -->
  <link rel="stylesheet" href="/plugins/pace/pace.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body class="hold-transition register-page">
<div class="row" style="margin-top: 20px">
    <div class="col-md-6 col-xs-12 col-sm-6 col-md-offset-3 col-sm-offset-3">
        <div class="register-logo">
            <a href="/">
            <img src="/images/ecell.png" height="100px"> <img src="/images/esummit_black.png" height="100px">
                <br><b>Entrepreneurship Cell</b> NIT Raipur
            </a>
        </div>
        <div class="register-box-body">
            <p class="login-box-msg">Create a new account</p>
            <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">Name</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('roll_no') ? ' has-error' : '' }}">
                    <label for="roll_no" class="col-md-4 control-label">ID No. or Roll No.</label>

                    <div class="col-md-6">
                        <input id="roll_no" type="text" class="form-control" name="roll_no" value="{{ old('roll_no') }}" required>

                        @if ($errors->has('roll_no'))
                            <span class="help-block">
                                <strong>{{ $errors->first('roll_no') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('branch') ? ' has-error' : '' }}">
                    <label for="branch" class="col-md-4 control-label">Branch</label>

                    <div class="col-md-6">
                        <input id="branch" type="text" class="form-control" name="branch" value="{{ old('branch') }}" required>

                        @if ($errors->has('branch'))
                            <span class="help-block">
                                <strong>{{ $errors->first('branch') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('contact_no') ? ' has-error' : '' }}">
                    <label for="contact_no" class="col-md-4 control-label">Contact No.</label>

                    <div class="col-md-6">
                        <input id="contact_no" type="text" class="form-control" name="contact_no" value="{{ old('contact_no') }}" required>

                        @if ($errors->has('contact_no'))
                            <span class="help-block">
                                <strong>{{ $errors->first('contact_no') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('institute') ? ' has-error' : '' }}">
                    <label for="institute" class="col-md-4 control-label">Institute</label>

                    <div class="col-md-6">
                        <input id="institute" type="text" class="form-control" name="institute" value="{{ old('institute') }}" required>

                        @if ($errors->has('institute'))
                            <span class="help-block">
                                <strong>{{ $errors->first('institute') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('semester') ? ' has-error' : '' }}">
                    <label for="semester" class="col-md-4 control-label">Semester</label>

                    <div class="col-md-6">
                        
                        <select class="form-control" name="semester">
                            <option value="I">1st Sem</option>
                            <option value="III">3rd Sem</option>
                            <option value="V">5th Sem</option>
                            <option value="VII">7th Sem</option>
                            <option value="">Others</option>
                        </select>                    

                        @if ($errors->has('semester'))
                            <span class="help-block">
                                <strong>{{ $errors->first('semester') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Password</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div>
                <center>
                    <div class="g-recaptcha" data-sitekey="6LcY6S0UAAAAADHnFNF8adPsTLL1Zzt_yyG71XjM"></div>
                </center>
                    <br>
                <center>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button type="submit" class="btn btn-primary btn-lg">
                                Register
                            </button>
                        </div>
                    </div>
                </center>
                <center>
                    <a href="/login" class="text-center">I already have an account</a>
                    <br>
                    <a href="/">&copy Entrepreneurship-Cell NIT Raipur</a>
                </center>
            </form>
        </div>
    </div>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../../plugins/iCheck/icheck.min.js"></script>
<!-- PACE -->
<script src="/bower_components/PACE/pace.min.js"></script>
<script>
    $(document).ajaxStart(function () {
        Pace.restart()
    })
    $(function () {
        $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
        });
    });
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-104074998-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>