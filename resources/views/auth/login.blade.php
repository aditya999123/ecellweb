<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>E-Cell | Login</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="/plugins/iCheck/square/blue.css">

  <link rel="icon" href="/images/ecell.png">
  <!-- Pace style -->
  <link rel="stylesheet" href="/plugins/pace/pace.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style>
  </style>
  <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body class="hold-transition register-page">
    <div class="row" style="margin-top: 20px">
        <div class="col-md-6 col-xs-12 col-sm-8 col-md-offset-3 col-sm-offset-2">
            <div class="register-logo">
                <a href="/">
                <img src="/images/ecell.png" height="100px"> <img src="/images/esummit_black.png" height="100px">
                    <br><b>Entrepreneurship Cell</b> NIT Raipur
                </a>
            </div>

            <div class="register-box-body" id="login_with_password_div">
                <p class="login-box-msg">Login</p>
                <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">Email</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Password</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}" required autofocus>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <center>
                        <div class="g-recaptcha" data-sitekey="6LcY6S0UAAAAADHnFNF8adPsTLL1Zzt_yyG71XjM"></div>
                    </center>
                    <br>
                    <center>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <button type="submit" class="btn btn-primary btn-lg">
                                    Login
                                </button>
                            </div>
                        </div>
                    </center>
                    <center>
                        <a onclick="login_with_otp()" class="text-center">Login with otp</a>
                    </center>
                    <center>
                        <a href="/register" class="text-center">Register</a>
                        <br>
                        <a href="/">&copy Entrepreneurship-Cell NIT Raipur</a>
                    </center>
                </form>
            </div>

            <div class="register-box-body" id="login_with_otp_div">
                <p class="login-box-msg">Create a new account</p>
                <div class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('contact_no') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">Contact no</label>
                        <div class="col-md-6">
                            <input id="contact_no" type="text" pattern="(7|8|9)\d{9}" class="form-control" name="email" value="{{ old('contact_no') }}" onkeypress="return isNumberKey(event)" required autofocus />
                                <span class="help-block">
                                    <strong id="send_otp_message"></strong>
                                </span>
                        </div>
                    </div>            
                    <center>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <button class="btn btn-primary btn-lg" onclick="send_otp()" id="send_otp">
                                    Send OTP
                                </button>
                            </div>
                        </div>
                    </center>
                    <center>
                        <a onclick="login_with_password()" class="text-center">Login with password</a>
                    </center>
                </div>
            </div>

            <div class="register-box-body" id="enter_otp_div">
                <p class="login-box-msg">Login with OTP</p>
                <div class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('otp') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">OTP</label>

                        <div class="col-md-6">
                            <input id="otp" type="text" class="form-control" name="otp" value="{{ old('otp') }}" onkeypress="return isNumberKey(event)" required autofocus />
                                <span class="help-block">
                                    <strong id="verify_otp_message"></strong>
                                </span>                    
                        </div>
                    </div>            
                    <center>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <button class="btn btn-primary btn-lg" onclick="verify_otp()" id="verify_otp_button">
                                    Verify OTP
                                </button>
                            </div>
                        </div>
                    </center>
                    <center>
                        <a onclick="login_with_password()" class="text-center">Login with password</a>
                    </center>
                </div>
            </div>
        </div>
    </div>
    <!-- /.form-box -->
    </div>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="/plugins/iCheck/icheck.min.js"></script>
<!-- PACE -->
<script src="/bower_components/PACE/pace.min.js"></script>
<script>
    $(document).ajaxStart(function () {
        Pace.restart()
    })
    $(document).ready(function(){
        $("#login_with_password_div").hide();
        $("#login_with_otp_div").hide();
        $("#enter_otp_div").hide();
        $("#login_with_password_div").fadeIn(2000);
        // $("#send_otp").click(function(event){
        //     event.preventDefault();
        // });
    });
    
    function login_with_otp(){
        $("#login_with_password_div").fadeOut(2000);
        $("#login_with_password_div").hide();
        $("#login_with_otp_div").fadeIn(2000);
        // alert('working');
    }

    function login_with_password(){
        $("#login_with_otp_div").fadeOut(2000);
        $("#login_with_otp_div").hide();
        $("#login_with_password_div").fadeIn(2000);
        // alert('working');
    } 

    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    function send_otp(){
        $("#send_otp_message").html('');
        var contact_no = $("#contact_no").val();
        if(contact_no.length == 10){
            $.ajax({
                url: "{{ route('send_otp_web') }}",
                method: 'POST',
                data: {
                    '_token' : '{{ csrf_token() }}',
                    'contact_no' : contact_no
                },
                success: function(data){
                    if(data.success == true){
                        $("#send_otp_message").html('OTP has been sent successfully');
                        $("#login_with_otp_div").fadeOut(2000);
                        $("#enter_otp_div").fadeIn(2000);
                    }else{
                        $("#send_otp_message").html('Sorry an error occured');
                    }
                },
                error: function(){
                    $("#send_otp_message").html('Sorry an error occured please try again after some time');
                }
            });
        }else{
            $("#send_otp_message").html('Please enter a vlid number');
        }
    }

    function verify_otp(){
        $("#verify_otp_message").html('');
        var otp = $("#otp").val();
        console.log(otp);
        var contact_no = $("#contact_no").val();
        if(otp.length == 4){
            $.ajax({
                url: "{{ route('verify_otp_web') }}",
                method: 'POST',
                data: {
                    '_token' : '{{ csrf_token() }}',
                    'otp' : otp,
                    'contact_no' : contact_no
                },
                success: function(data){
                    if(data.success == true){
                        $("#verify_otp_message").html('OTP has been sent successfully');
                        $("#enter_otp_div").fadeOut(2000);
                        $(location).attr('href','/bquiz');
                        console.log("OPT verified");
                    }else{
                        $("#verify_otp_message").html('Sorry an error occured');
                        console.log("OPT not verified");
                    }
                },
                error: function(){
                    $("#verify_otp_message").html('Sorry an error occured please try again after some time');
                    console.log("error function");
                }
            });
        }else{
            $("#verify_otp_message").html('Please enter a valid number');
        }
    }

  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-104074998-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>