<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>E-Cell | Verify Your Phone</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/dist/css/AdminLTE.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style>
    body {
        overflow : hidden;
    }
  </style>
</head>
<body class="hold-transition lockscreen">
    <!-- Automatic element centering -->
    <div class="lockscreen-wrapper">
        <div class="lockscreen-logo">
            <a href="#">
                <img src="/images/ecell.png" height="100px"> <img src="/images/esummit_black.png" height="100px">
                <br><b>Entrepreneurship Cell</b> NIT Raipur
            </a>
        </div>
        <!-- User name -->
        <div class="lockscreen-name">{{ Auth::user()->name }}</div>

        <!-- START LOCK SCREEN ITEM -->
        <div class="lockscreen-item">
            <!-- lockscreen image -->
            <div class="lockscreen-image">
            <img src="/storage/{{ Auth::user()->avatar }}" alt="User Image">
            </div>
            <!-- /.lockscreen-image -->

            <!-- lockscreen credentials (contains the form) -->
            <form class="lockscreen-credentials">
            <div class="input-group">
                <input type="password" name="otp" id="otp_field" class="form-control" placeholder="Enter Your OTP">

                <div class="input-group-btn">
                <button type="button" class="btn" onclick="submit_otp()"><i class="fa fa-arrow-right text-muted"></i></button>
                </div>
            </div>
            </form>
            <!-- /.lockscreen credentials -->

        </div>
        <!-- /.lockscreen-item -->
        <div class="help-block text-center" id="resend_otp_button">
            <a href="#" onclick="resend_otp()">Click here to resend OTP</a>
        </div>
        <div class="text-center">
            <a href="">Or sign in as a different user</a>
        </div>
        <div class="lockscreen-footer text-center">
            Copyright &copy; 2017 <b><a href="https://ecell.nitrr.ac.in" class="text-black">Entrepreneurship Cell NIT Raipur</a></b><br>
            All rights reserved
        </div>
    </div>
    <!-- /.center -->
    <div class="modal fade" id="modal-message">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Message from the page</h4>
                </div>
                <div class="modal-body">
                    <center><p id="message"></p></center>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>                    
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    <!-- /.modal-dialog -->
    </div>
</body>

<!-- jQuery 3 -->
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script>
    var i = 0;
    function timer_func(){
        i = i + 1;
        if(i == 31){
            clearIntereval(timer);
            $("#resend_otp_button").show();
        }
    }
    function resend_otp(){
        $.ajax({
            url: '{{ route('resend_otp_web') }}',
            success: function(data){
                $("#model-message").removeClass('modal-warning');
                $("#model-message").removeClass('modal-success');
                if(data.success == true){
                    i = 0;
                    $("#modal-message").addClass('modal-success');
                    $("#message").html(data.message);
                    $("#modal-message").modal('show');
                    $("#resend_otp_button").hide();
                    var timer = setInterval(function (){ timer_func() },1000);
                }else{
                    $("#modal-message").addClass('modal-warning');
                    $("#message").html(data.message);
                    $("#modal-message").modal('show');
                }
            },
            error: function(){
                $("#model-message").removeClass('modal-warning');
                $("#model-message").removeClass('modal-success');
                $("#modal-message").addClass('modal-warning');
                $("#message").html("Sorry an error occured!!! Please try again after some time :(");
                $("#modal-message").modal('show');
            }
        });
    }

    function submit_otp(){
        $.ajax({
            url : "{{ route('verify_otp_web') }}",
            type:'POST',
            data: {
                '_token' : '{{ csrf_token() }}',
                'otp' : $("#otp_field").val()
            },
            success: function(data){
                if(data.success == true){
                    $("#modal-message").addClass('modal-success');
                    $("#message").html(data.message);
                    $("#modal-message").modal('show');
                    $(location).attr('href', '/bquiz');
                }else{
                    $("#modal-message").addClass('modal-warning');
                    $("#message").html(data.message);
                    $("#modal-message").modal('show');
                }
            },
            error: function(){
                $("#model-message").removeClass('modal-warning');
                $("#model-message").removeClass('modal-success');
                $("#modal-message").addClass('modal-warning');
                $("#message").html("Sorry an error occured!!! Please try again after some time :(");
                $("#modal-message").modal('show');
            }
        });
    }
</script>
</body>
</html>