<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>E-Cell | @yield('title')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Favicon -->
  <link rel="icon" href="/images/ecell.png">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Pace style -->
  <link rel="stylesheet" href="/plugins/pace/pace.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style>
  .bootstrap-timepicker-widget table td:not(.separator) {
    min-width: 30px;
    color: black;
}
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="/" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>E</b>Cell</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Entrepreneurship</b>Cell</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="/storage/{{ Auth::user()->avatar }}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{ Auth::user()->name }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="/storage/{{ Auth::user()->avatar }}" class="img-circle" alt="User Image">

                <p>
                  {{ Auth::user()->name }} - {{ Auth::user()->user_type }}
                  <small>Member since {{ Auth::user()->created_at->month }}. {{ Auth::user()->created_at->year }}</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
      
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="/images/esummit_black.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Entrepreneurship Cell</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li id="dashboard_navbar">
          <a href="/admin">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li id="contact_us_navbar">
          <a href="/admin/contact_us">
            <i class="fa fa-commenting"></i> <span>Messages</span>
          </a>
        </li>
        <li>
          <a href="#" data-toggle="modal" data-target="#send_bulk_message_modal"><i class="fa fa-circle-o"></i> Send Bulk Message</a>
        </li>
        <li class="treeview" id="app_versions_navbar">
          <a href="#">
            <i class="fa fa-android"></i>
            <span>App Versions</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('admin_app_version') }}"><i class="fa fa-circle-o"></i> All Versions</a></li>
            <li><a href="#" data-toggle="modal" data-target="#add_app_version_modal"><i class="fa fa-circle-o"></i> Add a new version</a></li>
          </ul>
        </li>
        <li class="treeview" id="events_navbar">
          <a href="#">
            <i class="ion ion-calendar"></i>
            <span>Events</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/admin/events"><i class="fa fa-circle-o"></i> All Events</a></li>
            <li><a href="/admin/events/add"><i class="fa fa-circle-o"></i> Add an event</a></li>
          </ul>
        </li>
        <li class="treeview" id="startups_navbar">
          <a href="#">
            <i class="ion ion-stats-bars"></i>
            <span>Startups</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/admin/startups"><i class="fa fa-circle-o"></i> All Startups</a></li>
            <li><a href="#" data-toggle="modal" data-target="#add_startup_modal" ><i class="fa fa-circle-o"></i> Add a Startup</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-rupee"></i>
            <span>Sponsors</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/admin/sponsors"><i class="fa fa-circle-o"></i> All Sponsors</a></li>
            <li><a href="#" data-toggle="modal" data-target="#add_sponsor_modal"><i class="fa fa-circle-o"></i> Add a Sponsor</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-bullhorn"></i>
            <span>Speakers</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('admin_speakers') }}"><i class="fa fa-circle-o"></i> All Speakers</a></li>
            <li><a href="#" data-toggle="modal" data-target="#add_speaker_modal"><i class="fa fa-circle-o"></i> Add a Speaker</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-bullhorn"></i>
            <span>Team Members</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('admin_team_members') }}"><i class="fa fa-circle-o"></i> All Members</a></li>
            <li><a href="#" data-toggle="modal" data-target="#add_team_member_modal"><i class="fa fa-circle-o"></i> Add a Member</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-question-circle"></i>
            <span>B Quiz</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('admin_question_sets') }}"><i class="fa fa-circle-o"></i> All Question Sets</a></li>
            <li><a href="#" data-toggle="modal" data-target="#add_questionSet_modal"><i class="fa fa-plus-circle"></i> Add A Question Set</a></li>
            <li><a href="{{ route('admin_questions') }}"><i class="fa fa-circle-o"></i> All Question</a></li>
            <li><a href="#" data-toggle="modal" data-target="#add_question_modal"><i class="fa fa-plus-circle"></i> Add A Question</a></li>
          </ul>
        </li>
        <li class="treeview" id="mentors_navbar">
          <a href="#">
            <i class="glyphicon glyphicon-blackboard"></i>
            <span>Mentors</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/admin/mentors"><i class="fa fa-circle-o"></i> All Mentors</a></li>
            <li><a href="#" data-toggle="modal" data-target="#add_mentor_modal"><i class="fa fa-plus-circle"></i> Add A Mentor</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  @section('content')
  @show
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.1
    </div>
    <strong>Copyright &copy; 2017 <a href="https://ecell.nitrr.ac.in">Entrepreneurship Cell NIT Raipur</a>.</strong> All rights
    reserved.
  </footer>

  
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>

<!-- Add Startup Modal -->
<div class="modal modal-info fade" id="add_startup_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form role="form" enctype="multipart/form-data" method="POST" action="{{ route('startups.store') }}">
            {{ csrf_field() }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Add a startup</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="event_title">Name</span>
                            <input type="text" class="form-control" placeholder="Event Title" name="name" required>
                        </div>
                    </div>
                </div><br>
                <div class="row">
                  <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="event_description">Owner</span>
                            <input type="text" class="form-control" placeholder="Enter Responsible Person Name" name="owner" required>
                        </div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="event_detail">Description</span>
                            <textarea placeholder="Event Details" class="form-control" rows="5" name="description" required></textarea>
                        </div>
                    </div>
                </div><br> 
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="event_detail">Address</span>
                            <textarea placeholder="Address" class="form-control" rows="5" name="address" required></textarea>
                        </div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="event_image">Image</span>
                            <input type="file" class="form-control" placeholder="Event Image" name="meta" required>
                        </div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="event_time">Contact No</span>
                            <input type="datetime" class="form-control" placeholder="Contact No" name="contact_no" required>
                        </div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="event_time">Contact Email</span>
                            <input type="email" class="form-control" placeholder="Sponsor Email" name="contact_email" required>
                        </div>
                    </div>
                </div><br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </form>
    </div>
  </div>
</div>
<!-- End Add Startup Modal -->

<!-- Add Speaker Modal -->
<div class="modal modal-info fade" id="add_speaker_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form role="form" enctype="multipart/form-data" method="POST" action="{{ route('speakers.store') }}">
            {{ csrf_field() }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Add a speaker</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="event_title">Name</span>
                            <input type="text" class="form-control" placeholder="Speaker Name" name="name" required>
                        </div>
                    </div>
                </div><br>                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="event_detail">Description</span>
                            <textarea placeholder="Speakers Description" class="form-control" rows="5" name="description" required></textarea>
                        </div>
                    </div>
                </div><br>                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="event_image">Image</span>
                            <input type="file" class="form-control" placeholder="Image" name="meta" required>
                        </div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="event_time">Contact No</span>
                            <input type="datetime" class="form-control" placeholder="Contact No" name="contact_no" required>
                        </div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="input-group">
                            <span class="input-group-addon" id="event_time">Year</span>
                            <input type="text" class="form-control" placeholder="Year" name="year" required>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group">
                            <span class="input-group-addon" id="event_time">Contact Email</span>
                            <input type="email" class="form-control" placeholder="Speaker Email" name="contact_email" required>
                        </div>
                    </div>
                </div><br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </form>
    </div>
  </div>
</div>
<!-- End Speaker Modal -->

<!-- Add Mentor Modal -->
<div class="modal modal-info fade" id="add_mentor_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form role="form" enctype="multipart/form-data" method="POST" action="{{ route('mentors.store') }}">
            {{ csrf_field() }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Add a Mentor</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="mentor_title">Name</span>
                            <input type="text" class="form-control" placeholder="Mentor Title" name="name" required>
                        </div>
                    </div>
                </div><br>                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="mentor_detail">Description</span>
                            <textarea placeholder="Mentor Details" class="form-control" rows="5" name="description" required></textarea>
                        </div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="mentor_detail">Details</span>
                            <textarea placeholder="Mentor Details" class="form-control" rows="5" name="details" required></textarea>
                        </div>
                    </div>
                </div><br>                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="mentor_image">Image</span>
                            <input type="file" class="form-control" placeholder="Mentor Image" name="meta" required>
                        </div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="mentor_time">Contact No</span>
                            <input type="text" class="form-control" placeholder="Contact No" name="contact_no" required>
                        </div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="mentor_time">Contact Email</span>
                            <input type="email" class="form-control" placeholder="Speaker Email" name="contact_email" required>
                        </div>
                    </div>
                </div><br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </form>
    </div>
  </div>
</div>
<!-- End Add Mentor Modal -->

<!-- Add Sponsor Modal -->
<div class="modal modal-info fade" id="add_sponsor_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form role="form" enctype="multipart/form-data" method="POST" action="{{ route('sponsors.store') }}">
            {{ csrf_field() }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Add a sponsor</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="sponsor_name">Name</span>
                            <input type="text" class="form-control" placeholder="Sponsor Name" name="name" required>
                        </div>
                    </div>
                </div><br>
                <div class="row">
                  <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="sponsor_type">Sponsor Type</span>          
                            <select class="form-control" name="sponsor_type">
                              <option value = "title">Title Sponsor</option>
                              <option value = "gold">Gold Sponsor</option>
                              <option value ="platinum">Platinum Sponsor</option>
                              <option value = "associate">Associate Sponsor</option>
                              <option value ="partner">Partner Sponsor</option>
                              <option value="others">Others</option>
                            </select>
                            <!-- <input type="text" class="form-control" placeholder="Enter Sponsor Type" name="sponsor_type" required> -->
                        </div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="sponsor_description">Description</span>
                            <textarea placeholder="Event Details" class="form-control" rows="5" name="description" required></textarea>
                        </div>
                    </div>
                </div><br> 
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="sponsor_address">Address</span>
                            <textarea placeholder="Address" class="form-control" rows="5" name="address" required></textarea>
                        </div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="sponsor_image">Image</span>
                            <input type="file" class="form-control" placeholder="Event Image" name="meta" required>
                        </div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="input-group">
                            <span class="input-group-addon" id="sponsor_contact_no">Contact No</span>
                            <input type="datetime" class="form-control" placeholder="Contact No" name="contact_no" required>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group">
                            <span class="input-group-addon" id="sponsor_contact_email">Contact Email</span>
                            <input type="email" class="form-control" placeholder="Sponsor Email" name="contact_email" required>
                        </div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="sponsor_website">Sponsor Website</span>
                            <input type="url" class="form-control" placeholder="Sponsor Website" name="website" required>
                        </div>
                    </div>
                </div><br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </form>
    </div>
  </div>
</div>
<!-- End Add Sponsor Modal -->

<!-- Add Team Members Modal -->
<div class="modal modal-info fade" id="add_team_member_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form role="form" enctype="multipart/form-data" method="POST" action="{{ route('team_members.store') }}">
            {{ csrf_field() }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Add a Member</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="event_title">Name</span>
                            <input type="text" class="form-control" placeholder="Member Title" name="name" required>
                        </div>
                    </div>
                </div><br>                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon">Position</span>
                            <select class="form-control" name="member_position">
                              <option value ="overall">Overall Co-Ordinator</option>
                              <option value ="head">Head Co-Ordinator</option>
                              <option value ="managers">Manager</option>
                              <option value="executive">Executive</option>
                            </select>
                        </div>
                    </div>
                </div><br>                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="event_image">Image</span>
                            <input type="file" class="form-control" placeholder="Member Image" name="meta" required>
                        </div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-addon" id="event_time">LinkedIn</span>
                            <input type="url" class="form-control" placeholder="LinkedIn" name="linkedIn" required>
                        </div>
                    </div>
                </div><br>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </form>
    </div>
  </div>
</div>
<!-- End Team Members Modal -->

<!-- Add Question set modal -->
<div class="modal modal-info fade" id="add_questionSet_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" enctype="multipart/form-data" method="POST" action="{{ route('questionSets.store') }}">
        {{ csrf_field() }}
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Create a question set</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-lg-6">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-object-group"></i></span>
                <input type="text" class="form-control" placeholder="Set Name" name="set_name" required>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-question-circle"></i></span>
                <input type="text" class="form-control" placeholder="Set Number" name="set_number" required>
              </div>
            </div>
          </div><br>
          <div class="row">
            <div class="col-lg-12">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-arrows-alt"></i></span>
                <textarea type="text" row=5 class="form-control" placeholder="Set Description" name="description" required></textarea>
              </div>
            </div>
          </div><br>
          <div class="row">
            <div class="col-lg-12">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-file-picture-o"></i></span>
                <input type="file" name="meta" class="form-control">
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-outline">Save changes</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- End Add Question set modal -->

<!-- Add Question modal -->
<div class="modal modal-info fade" id="add_question_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" enctype="multipart/form-data" method="POST" action="{{ route('questions.store') }}">
        {{ csrf_field() }}
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Create A Question</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-lg-12">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-question-circle"></i></span>
                <input type="text" class="form-control" placeholder="Question" name="question" required>
              </div>
            </div>
          </div><br>
          <div class="row">
            <div class="col-lg-12">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-arrows-alt"></i></span>
                <textarea type="text" row=5 class="form-control" placeholder="Question Description" name="description" required></textarea>
              </div>
            </div>
          </div><br>
          <div class="row">
            <div class="col-lg-6">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                <input type="text" row=5 class="form-control" placeholder="Question Score" name="score" required>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-file-picture-o"></i></span>
                <input type="file" name="meta" class="form-control" multiple>
              </div>
            </div>
          </div><br>
          <div class="row">
            <div class="col-lg-6">
              <div class="input-group">
                <label>Select Question Set</label>
                <select class="form-control" name="question_set" id="question_sets_field">
                
                </select> 
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label>Time Limit</label>
                <div class="row">
                    <div class="col-lg-6">
                      <select class="form-control" name="minutes">
                        @for ($i = 0; $i <= 60; $i++)
                          <option value="{{ $i }}">{{ $i }} Minutes</option>
                        @endfor
                      </select>
                    </div>
                    <div class="col-lg-6">
                      <select class="form-control" name="seconds">
                        @for ($i = 0; $i <= 59; $i++)
                          <option value="{{ $i }}">{{ $i }} Seconds</option>
                        @endfor
                      </select>
                    </div>
                </div>
              </div>
            </div>
          </div><br>
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group">
                <label>Question Type</label>
                <select class="form-control" name="question_type">
                  <option value="text">Text</option>
                  <option value="image">Image</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-outline">Save changes</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- End Add Question modal -->

<!-- ./wrapper -->
<div class="modal fade modal-success" tabindex="-1" role="dialog" id="message_modal">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Message</h4>
            </div>
            <div class="modal-body">
                <p id="message_call"></p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-success" tabindex="-1" role="dialog" id="send_bulk_message_modal">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Enter Mobile Number</h4>
              </div>
              <div class="modal-body">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-file-picture-o"></i></span>
                        <textarea placeholder="Message" class="form-control" rows="5" id="message" name="mesaga" required></textarea>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <button type="submit" onclick="send_bulk_message()" class="btn btn-outline">Save changes</button>
              </div>
        </div>
    </div>
</div>

<div class="modal fade modal-success" tabindex="-1" role="dialog" id="add_app_version_modal">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form method="POST" action="{{ route('admin_add_app_version') }}">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Update version</h4>
              </div>
              <div class="modal-body">
                  {{ csrf_field() }}
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-android"></i></span>
                        <input type="text" name="version" class="form-control" placeholder="Enter new version">
                      </div>
                    </div>
                  </div><br>
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-code-fork"></i></span>
                        <input type="text" name="change_log" class="form-control" placeholder="Enter Changlog">
                      </div>
                    </div>
                  </div><br>
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa  fa-code-fork"></i></span>
                        <input type="text" name="url" class="form-control" placeholder="Update url">
                      </div>
                    </div>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-outline">Save changes</button>
              </div>
            </form>
        </div>
    </div>
</div>
<!-- jQuery 3 -->
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="/bower_components/raphael/raphael.min.js"></script>
<script src="/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/dist/js/demo.js"></script>
<script src="/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- DataTables -->
<script src="/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- PACE -->
<script src="/bower_components/PACE/pace.min.js"></script>
<script>
  $(document).ajaxStart(function () {
        Pace.restart()
    })
  $(function (){
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //Time Picker
    $('.timepicker').timepicker({
      showInputs: false
    })

    $('#example1').DataTable()
  })
</script>
@section("scripts")
@show

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-104074998-1', 'auto');
  ga('send', 'pageview');

</script>
<script>
  function send_bulk_message(){
    alert("jsgvbhdkn");
    $.ajax({
      url: '{{ route("send_bulk_messages") }}',
      data: {
        'message' : $("#message").val(),
        '_token' : '{{ csrf_token() }}'
      },
      method: 'POST',
      success: function(data){
        if(data.success == true){
          alert(data.message);
        }else{
          alert(data.message);
        }
        console.log($("#message").val());
      },
      error: function(){
        alert('Sorry an error occured');
        console.log('URL not accessible');
      }
    });
  }
  $(document).ready(function(){
    $.ajax({
      url: '/admin/get_question_sets_for_question',
      success:function(data){
        var select = '';
        if(data.success == true){
          for(i in data.question_sets){
            select += "<option value=\""+data.question_sets[i]['id']+"\">"+data.question_sets[i]['name']+"</option>";
            console.log(data.question_sets[i]);
          }          
          $("#question_sets_field").html(select);
        }
      },
      error: function(){
        alert('Seems like your are not connected to the internet');
      }
    });
  });
  
</script>
@section('script')
@show
</body>
</html>
