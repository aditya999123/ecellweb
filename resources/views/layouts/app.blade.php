<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>E-Summit '17 | @yield('title')</title>
    <link rel="shortcut icon" href="/images/logo-final.png" type="image/x-icon">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Owl -->
    <link rel="stylesheet" type="text/css" href="/css/owl.css">
    <!-- Animate.css -->
    <link rel="stylesheet" type="text/css" href="/css/animate.css">
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:200,700" rel="styesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Main style -->
    <link rel="stylesheet" type="text/css" href="/css/main.css">
    <!-- Responsive -->
    <link href="/css/responsive.css" rel="stylesheet">
    <!-- Login Modal -->
    <link href="/css/login.css" rel="stylesheet">
    <!-- Carousel -->
    <link href="/css/carousel.css" rel="stylesheet">
    <!-- Events -->
    <link href="/css/events.css" rel="stylesheet">
    <!--Font awesome -->
    <link href="/css/font-awesome.css" rel="stylesheet" type="text/css">

    <!-- for Facebook -->          
    <meta property="og:title" content="E-Cell | @yield('title')" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content="@yield('image')" />
    <meta property="og:url" content="https://ecell.nitrr.ac.in/@yield('page_link')" />
    <meta property="og:description" content="Entrepreneurship Cell of NIT Raipur is non-profit organization working towards promoting the spirit of Entrepreneurship across Chhattisgarh. It aims to promote a startup-culture among denizens of NIT Raipur and equip them with the necessary tools to do so." />
    <meta property="fb:app_id" content="1687828501526245">
    <!-- Scripts -->
    <script src="/js/jquery-1.11.1.min.js"></script>
    <script src="/js/dots.js"></script>
    <script src="/js/modernizr.min.js"></script>
    <script src="/js/owl.carousel.min.js"></script>
    <script src="/js/typed.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="/js/jquery.onepagenav.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/login.js"></script>
</head>

<body>
    <!--Modal Login/Signup-->
    <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" align="center">
                    <img src="/images/login.png">
                </div>
                <div id="div-forms">
                    <form id="login-form" method="post" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="modal-body">
                            <input id="login_username" class="form-control" type="text" placeholder="Username" required>
                            <input id="login_password" class="form-control" type="password" placeholder="Password" required>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"> Remember me
                                </label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div>
                                <button type="submit" class="btn btn-success btn-lg btn-block">Login</button>
                            </div>
                            <center>
                                <div>
                                    <button id="login_lost_btn" type="button" class="btn btn-link">Lost Password?</button>
                                    <button id="login_register_btn" type="button" class="btn btn-link">Register</button>
                                </div>
                            </center>
                            <br>
                            <center>
                                <h4>OR</h4><br>
                                <div class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></div>
                            </center>
                        </div>
                    </form>
                    <form id="lost-form" style="display:none;">
                        <div class="modal-body">
                            <input id="lost_email" class="form-control" type="text" placeholder="E-Mail" required>
                        </div>
                        <div class="modal-footer">
                            <div>
                                <button type="submit" class="btn btn-success btn-lg btn-block">Send</button>
                            </div>
                            <div>
                                <button id="lost_login_btn" type="button" class="btn btn-link">Log In</button>
                                <button id="lost_register_btn" type="button" class="btn btn-link">Register</button>
                            </div>
                        </div>
                    </form>
                    <form id="register-form" style="display:none;">
                        <div class="modal-body">
                            <input id="register_username" class="form-control" type="text" placeholder="Username" required>
                            <input id="register_email" class="form-control" type="text" placeholder="E-Mail" required>
                            <input id="register_password" class="form-control" type="password" placeholder="Password" required>
                        </div>
                        <div class="modal-footer">
                            <div>
                                <button type="submit" class="btn btn-success btn-lg btn-block">Register</button>
                            </div>
                            <div>
                                <button id="register_login_btn" type="button" class="btn btn-link">Log In</button>
                                <button id="register_lost_btn" type="button" class="btn btn-link">Lost Password?</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- NavBar -->
    <nav class="navbar sidebar" role="navigation">
        <div class="container-fluid">
            <center>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar" style="background-color:white;"></span>
                        <span class="icon-bar" style="background-color:red;"></span>
                        <span class="icon-bar" style="background-color:white;"></span>
                    </button>
                    <img class="logo" src="/images/logo-final.png" style="padding-top:10px;">
                    <a class="navbar-brand" href="#" style="padding:10px;">       E-Cell NIT Raipur</a>
                </div>
            </center>
            <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="/">Home<span id="home_navbar" style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-home red"></span></a></li>
                    <li><a href="/vision">Vision<span id="vision_navbar" style="font-size:16px;" class="pull-right hidden-xs showopacity 	glyphicon glyphicon-education"></span></a></li>
                    <li><a href="/events">Events<span id="events_navbar" style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-dashboard"></span></a></li>
                    <li><a href="/team">Team<span id="team_navbar" style="font-size:16px;" class="pull-right hidden-xs showopacity 	glyphicon glyphicon-user"></span></a></li>
                    <li><a href="/sponsors">Sponsors<span id="sponsors_navbar" style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-usd"></span></a></li>
                    <li><a href="/mentors">Mentors<span id="mentors_navbar" style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-blackboard"></span></a></li>
                    <li><a href="/speakers">Speakers<span id="speakers_navbar" style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-bullhorn"></span></a></li>
                    <li><a href="/blogs">Blogs<span id="blogs_navbar" style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-dashboard"></span></a></li>
                    @if(Auth::user())
                        <li><a href="/bquiz">B-Quiz<span id="blogs_navbar" style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-play"></span></a></li>
                        <li><a href="/logout">Logout<span id="logout" style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-log-out"></span></a></li>
                    @else
                        <li><a href="/login">Login/Sign-Up<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon glyphicon-log-in"></span></a></li>
                    @endif
                </ul>
                </li>
                </ul>
            </div>
        </div>
    </nav>
    <!--Main Content starts-->
    <div class="main">
        <!-- <div class="scrollbar" id="scrollbar-custom"> -->
            <!-- <div class="force-overflow"></div> -->
                @section('content')
                @show
                <section class="footer">
                    <center>
                        <h5><strong>© E-Cell, NIT Raipur</strong></h5></center>
                </section>
            </div>
        <!-- </div> -->
    </div>
    @section("scripts")
    @show
    <script>
        $(document).ready(function () {
            if (!$.browser.webkit) {
                $('.wrapper').html('<p>Sorry! Non webkit users. :(</p>');
            }
        });
    </script>
</body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-104074998-1', 'auto');
  ga('send', 'pageview');

</script>
</html>