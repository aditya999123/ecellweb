@extends('layouts.app')
@section('title', 'Speakers')

@section('content')
@parent
<link rel="stylesheet" href="/css/speaker.css">
<section id="speakers" style="padding:50px 15px;text-align:left;">
    <div class="row">
        <h1 class="text-center">Our Speakers</h1>
        <hr style="width:30%;">
    </div>
    <center>
        <div class="row" id="speakers_list">
        </div>
    </center>

    <div class="row">
        <h1 class="text-center">Previous Speakers</h1>
        <hr style="width:30%;">
    </div>
    <center>
        <div class="row" id="previous_year_speakers_list">
        </div>
    </center>

</section>
@endsection

@section('scripts')
<script>
    $("#speakers_navbar").css({'color': 'white'});
    var dt = new Date();
    var current_year = dt.getFullYear();
    $.ajax({
        url: '{{ route("get_speakers_list") }}',
        success:function(data){
            var previous_speakers = '';
            var speakers = '';
            var p_spkr_counter = 0;
            var spkr_counter = 0;
            if(data.success == true){
                for(i in data.speakers){
                    if(data.speakers[i]['year'] == current_year){
                        spkr_counter = spkr_counter + 1;
                        speakers += "<div class=\"col-sm-6 col-md-4 col-lg-4 mt-4\">";
                        speakers += "<div class=\"card\">";
                        speakers += "<div class=\"card-content\">";
                        speakers += "<img class=\"card-img-top\" src=\""+data.speakers[i]['image']+"\">";
                        speakers += "<div class=\"card-block\">";
                        speakers += "<h4 class=\"card-title text-primary text-center\">"+data.speakers[i]['name']+"</h4>";                    
                        speakers += "<div class=\"card-text\"><center>";
                        speakers += data.speakers[i]['description'];
                        speakers += "</center></div></div></div></div></div>";
                        if(parseInt(spkr_counter)%3 == 0 && i != 0){
                            speakers += "</div>";
                            speakers += "<div class=\"row\">";                                            
                        }    
                    }else{
                        p_spkr_counter = p_spkr_counter + 1;
                        previous_speakers += "<div class=\"col-sm-6 col-md-4 col-lg-4 mt-4\">";
                        previous_speakers += "<div class=\"card\">";
                        previous_speakers += "<div class=\"card-content\">";
                        previous_speakers += "<img class=\"card-img-top\" src=\""+data.speakers[i]['image']+"\">";
                        previous_speakers += "<div class=\"card-block\">";
                        previous_speakers += "<h4 class=\"card-title text-primary text-center\">"+data.speakers[i]['name']+"</h4>";                    
                        previous_speakers += "<div class=\"card-text\"><center>";
                        previous_speakers += data.speakers[i]['description'];
                        previous_speakers += "</center></div></div></div></div></div>";
                        if(parseInt(p_spkr_counter)%3 == 0 && i != 0){
                            previous_speakers += "</div>";
                            previous_speakers += "<div class=\"row\">";                                            
                        }
                    }
                }                
                $("#previous_year_speakers_list").html(previous_speakers);
                $("#speakers_list").html(speakers);
            }
            else{
                previous_speakers += "<center><h2>"+data.message+"</h2></center>";
                $("#previous_year_speakers_list").html(speakers);
            }
        },
        error:function(){
            alert('Sorry an error occured :(');
        }
    });
</script>
@endsection
